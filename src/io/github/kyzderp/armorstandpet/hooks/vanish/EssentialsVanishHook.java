/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.hooks.vanish;

import io.github.kyzderp.armorstandpet.ASPetPlugin;

import org.bukkit.entity.Player;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;

public class EssentialsVanishHook implements IVanishHook
{
	private Essentials plugin;
	
	public EssentialsVanishHook(Essentials plugin)
	{
		this.plugin = plugin;
		ASPetPlugin.getInstance().getLogger().info("Detected Essentials. Enabling hook;"
				+ " doormen will not greet vanished players.");
	}

	@Override
	public boolean isVanished(Player player) 
	{
		User user = this.plugin.getUser(player);
		if (user == null)
			return false;
		return user.isVanished();
	}
}
