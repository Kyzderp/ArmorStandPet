/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.hooks.vanish;

import io.github.kyzderp.armorstandpet.ASPetPlugin;

import org.bukkit.entity.Player;
import org.kitteh.vanish.VanishManager;
import org.kitteh.vanish.VanishPlugin;

public class VanishNoPacketHook implements IVanishHook
{
	private VanishManager vanishManager;
	
	public VanishNoPacketHook(VanishPlugin plugin)
	{
		this.vanishManager = plugin.getManager();
		ASPetPlugin.getInstance().getLogger().info("Detected VanishNoPacket. Enabling hook;"
				+ " doormen will not greet vanished players.");
	}

	@Override
	public boolean isVanished(Player player) 
	{
		return this.vanishManager.isVanished(player);
	}
}
