/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.hooks.vanish;

import org.bukkit.entity.Player;

public interface IVanishHook 
{
	/**
	 * Checks whether the player is vanished according to this plugin
	 * @param player
	 * @return
	 */
	public boolean isVanished(Player player);
}
