/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.hooks;

public class HookManager 
{
	public static BuildingCheck buildingCheck;
	public static VanishCheck vanishCheck;
	
	public HookManager()
	{
		HookManager.buildingCheck = new BuildingCheck();
		HookManager.vanishCheck = new VanishCheck();
	}
}
