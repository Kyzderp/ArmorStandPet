/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.hooks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class BuildingCheck 
{
	/**
	 * Check if the player can place an armor stand in this location
	 * Fires a fake event, so it may result in messages from other plugins
	 * @param player
	 * @param loc
	 * @return
	 */
	public boolean canPlaceArmorStand(Player player, Location loc)
	{
		PlayerInteractEvent fakeEvent = new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, 
				new ItemStack(Material.ARMOR_STAND),
				loc.subtract(0, 1, 0).getBlock(), BlockFace.UP);
		
		Bukkit.getPluginManager().callEvent(fakeEvent);
		
		if (fakeEvent.isCancelled())
		{
			fakeEvent.setCancelled(true);
			return false;
		}
		
		fakeEvent.setCancelled(true);
		return true;
	}
}
