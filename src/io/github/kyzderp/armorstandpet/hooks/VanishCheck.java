/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.hooks;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.hooks.vanish.EssentialsVanishHook;
import io.github.kyzderp.armorstandpet.hooks.vanish.IVanishHook;
import io.github.kyzderp.armorstandpet.hooks.vanish.VanishNoPacketHook;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.kitteh.vanish.VanishPlugin;

import com.earth2me.essentials.Essentials;

public class VanishCheck 
{
	private List<IVanishHook> vanishHooks;
	
	public VanishCheck()
	{
		ASPetPlugin plugin = ASPetPlugin.getInstance();
		
		this.vanishHooks = new ArrayList<IVanishHook>();
		
		// VanishNoPacket
		if (plugin.getServer().getPluginManager().isPluginEnabled("VanishNoPacket"))
			this.vanishHooks.add(new VanishNoPacketHook((VanishPlugin) 
					plugin.getServer().getPluginManager().getPlugin("VanishNoPacket")));
		
		// Essentials
		if (plugin.getServer().getPluginManager().isPluginEnabled("Essentials"))
			this.vanishHooks.add(new EssentialsVanishHook((Essentials)
					plugin.getServer().getPluginManager().getPlugin("Essentials")));
	}
	
	/**
	 * Checks if the player is vanished in any of the available vanish plugins
	 * @param player
	 * @return
	 */
	public boolean isVanished(Player player)
	{
		for (IVanishHook hook: this.vanishHooks)
		{
			if (hook.isVanished(player))
				return true;
		}
		return false;
	}
}
