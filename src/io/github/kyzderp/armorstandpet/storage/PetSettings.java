/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.storage;

import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.HashMap;
import java.util.Map;

public class PetSettings 
{
	private String owner;
	private Map<Pet, String> pets;
	
	public PetSettings(String owner)
	{
		this.owner = owner;
		this.pets = new HashMap<Pet, String>();
	}
	
	public PetSettings(String owner, Pet pet, String world)
	{
		this.owner = owner;
		this.pets = new HashMap<Pet, String>();
		this.pets.put(pet, world);
	}
	
	public PetSettings(String owner, HashMap<Pet, String> pets)
	{
		this.owner = owner;
		this.pets = pets;
	}
	
	public String getOwner()
	{
		return this.owner;
	}
	
	public Map<Pet, String> getPets()
	{
		return this.pets;
	}
	
	public void addPet(Pet pet, String world)
	{
		this.pets.put(pet, world);
	}
}
