/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.storage;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.ASPetUtils;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.struct.UnloadedPets;
import io.github.kyzderp.armorstandpet.types.Pet;
import io.github.kyzderp.armorstandpet.types.PetType;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Chunk;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

public class PetStorage 
{
	private ASPetPlugin plugin;

	public PetStorage(ASPetPlugin plugin)
	{
		this.plugin = plugin;
	}

	public void saveAllPets(boolean removeStands)
	{
		// Need a map of settings
		Map<String, PetSettings> petSettings = new HashMap<String, PetSettings>();

		for (String world: StandToOwner.getWorlds())
		{
			Map<ArmorStand, String> innermap = StandToOwner.getWorld(world);
			for (ArmorStand stand: innermap.keySet())
			{
				String owner = innermap.get(stand);
				Pet pet = OwnerToPet.get(world, owner);

				// Something's wrong, the stand isn't the pet
				if (stand == null || pet == null || !stand.equals(pet.getStand()))
				{
					this.plugin.getLogger().warning(owner + "'s pet in "
							+ world + " did not match armor stand");
					continue;
				}

				if (removeStands)
					stand.remove();

				PetSettings settings = petSettings.get(owner);
				if (settings == null)
				{
					// Does not exist yet, create
					petSettings.put(owner, new PetSettings(owner, pet, world));
				}
				else
				{
					// Already exists, just add the pet
					settings.addPet(pet, world);
				}
			}
		}

		// Done compiling all of the pets, now save them one by one
		for (PetSettings settings: petSettings.values())
		{
			this.savePets(settings);
		}

		// The pets we just saved are the active ones.
		// Keep track of active pet: owner, world, type
		YamlConfiguration config;
		File file = new File(this.plugin.getDataFolder(), "pets.yml");
		config = new YamlConfiguration();

		for (String owner: petSettings.keySet())
		{
			PetSettings settings = petSettings.get(owner);
			for (Pet pet: settings.getPets().keySet())
			{
				String world = settings.getPets().get(pet);
				config.set("active." + world + "." + owner, pet.type.name);
			}
		}

		if (petSettings.size() == 0)
			config.set("active", "EMPTY");

		// Save the main file
		try {
			config.save(file);
			this.plugin.getLogger().info("Saved " + petSettings.size() 
					+ " active pets in pets.yml");
		} catch (IOException e) {
			this.plugin.getLogger().severe("Unable to save active pets!");
		}
	}

	public void savePets(PetSettings petSettings)
	{
		// Get the right file first
		String owner = petSettings.getOwner();

		File dir = new File(this.plugin.getDataFolder(), "pets");
		dir.mkdirs();
		File petFile = new File(dir, owner + ".yml");

		YamlConfiguration config;
		if (petFile.exists())
		{
			// Config already exists, take it and just overwrite new ones
			config = YamlConfiguration.loadConfiguration(petFile);
		}
		else
		{
			// No existing config, use a new one
			config = new YamlConfiguration(); 
		}

		// Now put it in yaml
		for (Pet pet: petSettings.getPets().keySet())
		{
			String worldname = petSettings.getPets().get(pet);
			String type = pet.type.name;
			config.set(worldname + "." + type, pet.serialize());
		}

		try {
			config.save(petFile);
			this.plugin.getLogger().info("Saved " + petSettings.getPets().size() 
					+ " pets in " + owner + ".yml");
		} catch (IOException e) {
			this.plugin.getLogger().warning("Unable to save pets for " + owner + "!");
		}
	}

	private void savePets(String owner, String world, ConfigurationSection section)
	{
		// Get the right file first
		File dir = new File(this.plugin.getDataFolder(), "pets");
		dir.mkdirs();
		File petFile = new File(dir, owner + ".yml");

		YamlConfiguration config;
		if (petFile.exists())
		{
			// Config already exists, take it and just overwrite new ones
			config = YamlConfiguration.loadConfiguration(petFile);
		}
		else
		{
			// No existing config, use a new one
			config = new YamlConfiguration(); 
		}

		// Now put it in yaml
		config.set(world + "." + section.getString("type"), section);

		try {
			config.save(petFile);
			this.plugin.getLogger().info("Migrated 1 pet to " + owner + ".yml");
		} catch (IOException e) {
			this.plugin.getLogger().warning("Unable to migrate pet for " + owner + "!");
		}
	}

	public void loadAll(boolean preserve)
	{
		YamlConfiguration config;
		File file = new File(this.plugin.getDataFolder(), "pets.yml");
		if (!file.exists())
			return;
		else
			config = YamlConfiguration.loadConfiguration(file);

		ConfigurationSection activeSection = config.getConfigurationSection("active");

		// If there is no "active" section, it may be the old storage
		// needs migrating
		if (config.getString("active") == null)
		{
			this.plugin.getLogger().warning("File storage is still using the old system (v1.6.2 or earlier),"
					+ " migrating to new system.");
			this.migrate(config);
			this.plugin.getLogger().info("Migration complete. Loading pets normally.");
			this.loadAll(preserve);
			return;
		}

		if (activeSection == null)
			return;

		for (String world: activeSection.getKeys(false))
		{
			ConfigurationSection section = activeSection.getConfigurationSection(world);
			for (String owner: section.getKeys(false))
			{
				// Active pet
				try 
				{
					PetType type = PetType.getTypeByName(section.getString(owner));
					if (type == null)
					{
						this.plugin.getLogger().warning("Unable to load pet for " + owner 
								+ " in " + world + "; type " + section.getString(owner)
								+ " is invalid.");
						continue;
					}
					
					// Load the specified pet from the owner's file
					Pet pet = this.loadPet(owner, world, type);

					pet.setStand(ASPetUtils.respawnStand(pet.getStand()));
					int removed = 0;
					if (Settings.removeDupes && !preserve)
						removed = this.removeDupes(pet.getStand());

					OwnerToPet.put(world, owner, pet);
					StandToOwner.put(world, pet.getStand(), owner);

					// Add to unloaded list
					Chunk chunk = pet.getStand().getLocation().getChunk();
					String chunkString = chunk.getX() + " " + chunk.getZ();

					UnloadedPets.putStand(world, chunkString, pet.getStand());

					String log = "Loaded pet for " + owner + " in " + world 
							+ " at chunk " + chunkString;
					if (removed > 0)
						log += " and removed " + removed + " possibly duplicated armor stand(s).";
					this.plugin.getLogger().info(log);
				} catch (PlayerFileDoesNotExistException e) {
					this.plugin.getLogger().info("Unable to load pet for " + owner 
							+ " in " + world + "; player file is missing.");
				} catch (PetConfigDoesNotExistException e) {
					this.plugin.getLogger().info("No pet config found for " + owner 
							+ " in " + world);
				} catch (WorldDoesNotExistException e) {
					this.plugin.getLogger().warning("Unable to spawn armor stand in world "
							+ world + ", does this world exist?");
				}
			}
		}
	}

	private int removeDupes(ArmorStand stand)
	{
		if (Settings.DEBUG)
			this.plugin.getLogger().info("Trying to remove dupes");
		int removed = 0;
		Entity[] entities = stand.getLocation().getChunk().getEntities();
		for (Entity entity: entities)
		{
			if (entity instanceof ArmorStand)
			{
				ArmorStand other = (ArmorStand) entity;
				// Don't remove the current, obviously
				if (stand.equals(other))
					continue;
				if (Settings.DEBUG)
					this.plugin.getLogger().info("Checking distance");
				// Must be close enough, 0.2m
				if (entity.getLocation().distanceSquared(stand.getLocation()) <= 0.04)
				{
					if (Settings.DEBUG)
						this.plugin.getLogger().info("Checking attributes");
					// Matching attributes and equipment
					if (stand.hasArms() == other.hasArms()
							&& stand.hasBasePlate() == other.hasBasePlate()
							&& stand.isVisible() == other.isVisible()
							&& stand.isSmall() == other.isSmall()
							&& stand.getHelmet().equals(other.getHelmet())
							&& stand.getChestplate().equals(other.getChestplate())
							&& stand.getLeggings().equals(other.getLeggings())
							&& stand.getBoots().equals(other.getBoots())
							&& stand.getItemInHand().equals(other.getItemInHand()))
					{
						if (Settings.DEBUG)
							this.plugin.getLogger().info("Remove");
						other.remove();
						removed++;
					}
				}
			}
		}
		return removed;
	}

	/**
	 * Migrate a config section from the old system
	 * @param config
	 */
	private void migrate(YamlConfiguration config) 
	{
		Map<String, String[]> active = new HashMap<String, String[]>();

		// Fix the individual files
		for (String world: config.getKeys(false))
		{
			ConfigurationSection worldSection = config.getConfigurationSection(world);
			for (String player: worldSection.getKeys(false))
			{
				ConfigurationSection playerSection = worldSection.getConfigurationSection(player);
				this.savePets(player, world, playerSection);
				active.put(player, new String[] {world, playerSection.getString("type")});
				/*String type = playerSection.getString("type");
				Pet pet = Pet.createPet(PetType.getTypeByName(type), player, null);
				pet.deserialize(playerSection);

				PetSettings settings = new PetSettings(player, pet, world);
				petSettings.put(player, settings);
				this.savePets(settings);*/
			}
		}

		// Now fix the pets file
		YamlConfiguration petConfig;
		File file = new File(this.plugin.getDataFolder(), "pets.yml");
		petConfig = new YamlConfiguration();

		for (String owner: active.keySet())
		{
			String world = active.get(owner)[0];
			String type = active.get(owner)[1];
			petConfig.set("active." + world + "." + owner, type);
		}

		if (active.size() == 0)
			petConfig.set("active", "EMPTY");

		// Save the main file
		try {
			petConfig.save(file);
			this.plugin.getLogger().info("Migrated " + active.size() 
					+ " active pets to pets.yml");
		} catch (IOException e) {
			this.plugin.getLogger().severe("Unable to migrate active pets!");
		}
	}

	/**
	 * Load and create the pet from the owner's file
	 * @param owner
	 * @param world
	 * @param type
	 * @return
	 * @throws PlayerFileDoesNotExistException
	 * @throws PetConfigDoesNotExistException
	 * @throws WorldDoesNotExistException 
	 */
	public Pet loadPet(String owner, String world, PetType type) 
			throws PlayerFileDoesNotExistException, PetConfigDoesNotExistException, WorldDoesNotExistException
	{
		// Get the right file first
		File dir = new File(this.plugin.getDataFolder(), "pets");
		dir.mkdirs();
		File petFile = new File(dir, owner + ".yml");

		if (!petFile.exists())
			throw new PlayerFileDoesNotExistException();

		// Now put it in yaml
		YamlConfiguration config = YamlConfiguration.loadConfiguration(petFile);

		Pet pet = Pet.createPet(type, owner, null);
		ConfigurationSection section = config.getConfigurationSection(world + "." + type.name);
		if (section == null)
			throw new PetConfigDoesNotExistException();
		pet.deserialize(section);

		if (pet.getStand() == null) // Failed to spawn the armor stand
			throw new WorldDoesNotExistException();

		return pet;
	}

	/**
	 * Load and create the pet from the owner's file, with existing armor stand
	 * @param owner
	 * @param world
	 * @param type
	 * @param stand
	 * @return
	 * @throws PlayerFileDoesNotExistException
	 * @throws PetConfigDoesNotExistException
	 */
	public Pet loadPetSettings(String owner, String world, PetType type, ArmorStand stand) 
			throws PlayerFileDoesNotExistException, PetConfigDoesNotExistException
	{
		// Get the right file first
		File dir = new File(this.plugin.getDataFolder(), "pets");
		dir.mkdirs();
		File petFile = new File(dir, owner + ".yml");

		if (!petFile.exists())
			throw new PlayerFileDoesNotExistException();

		// Now put it in yaml
		YamlConfiguration config = YamlConfiguration.loadConfiguration(petFile);

		Pet pet = Pet.createPet(type, owner, null);
		ConfigurationSection section = config.getConfigurationSection(world + "." + type.name);
		if (section == null)
			throw new PetConfigDoesNotExistException();
		pet.setStand(stand);
		pet.deserializeSettings(section);

		return pet;
	}
}
