/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.gui;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class ASPetGUIListener implements Listener 
{
	private Map<String, ChooseTypeGUI> playerToGUI;
	
	public ASPetGUIListener()
	{
		this.playerToGUI = new HashMap<String, ChooseTypeGUI>();
	}
	
	@EventHandler
	public void onPlayerPickItem(InventoryClickEvent event)
	{
		if (event.getWhoClicked() instanceof Player
				&& event.getInventory().getTitle().equals("ArmorStandPet Type"))
		{
			event.setCancelled(true);
			
			if (!event.getClickedInventory().getTitle().equals("ArmorStandPet Type"))
				return;
			
			String name = event.getWhoClicked().getName();
			ASPetPlugin.getInstance().getLogger().info("Attempting to get GUI for " + name);
			
			ChooseTypeGUI gui = this.playerToGUI.get(name);
			if (gui == null)
			{
				ASPetPlugin.error(event.getWhoClicked(), "Something went wrong, try again.");
				
				if (Settings.DEBUG)
				{
					String log = "playerToGUI: ";
					for (String key: this.playerToGUI.keySet())
						log += key + " ";
					ASPetPlugin.getInstance().getLogger().info(log);
				}
				
				return;
			}
			
			boolean close = gui.handleClick(event);
			if (close)
				this.playerToGUI.remove(event.getWhoClicked().getName());
		}
	}
	
	@EventHandler
	public void onCloseInventory(InventoryCloseEvent event)
	{
		this.getPlayerToGUI().remove(event.getPlayer().getName());
	}
	
	public Map<String, ChooseTypeGUI> getPlayerToGUI()
	{
		return this.playerToGUI;
	}
}
