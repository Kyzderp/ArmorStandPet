/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.gui;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.storage.PetConfigDoesNotExistException;
import io.github.kyzderp.armorstandpet.storage.PetSettings;
import io.github.kyzderp.armorstandpet.storage.PlayerFileDoesNotExistException;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.types.Pet;
import io.github.kyzderp.armorstandpet.types.PetType;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ChooseTypeGUI
{
	private ASPetPlugin plugin;
	private ArmorStand stand;
	private Inventory inventory;
	private Player player;

	/*
	 * TODO
	 * Say we already have settings saved for a needy child that has the name Bob. Currently we have
	 * a doorman called Max. When crouch-clicking on the doorman to change the aspet's type to needy
	 * child, should the name become Bob, or stay as Max? In other words, when there are existing
	 * settings for a pet type, should the saved settings overwrite the current settings, or should
	 * the current settings try to apply themselves to the new pet type and only fill in undefined
	 * ones from the saved settings? I'm more inclined to go with the former since it seems to make
	 * more sense for a regular player using it, but I can see why you might want the latter
	 * sometimes too. Maybe there should be an option.
	 */

	public ChooseTypeGUI(ASPetPlugin plugin, Player player, ArmorStand stand)
	{
		this.plugin = plugin;
		this.player = player;
		this.stand = stand;
		this.inventory = Bukkit.createInventory(null, InventoryType.HOPPER, "ArmorStandPet Type");
		this.initializeInventory();
	}

	public boolean handleClick(InventoryClickEvent event)
	{
		ItemStack stack;
		try {
			stack = this.inventory.getItem(event.getSlot());
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		
		if (stack == null || stack.getType() == Material.AIR)
			stack = event.getCurrentItem();
		
		if (stack == null || stack.getType() == Material.AIR)
			return false;
		
		if (!stack.hasItemMeta())
			return false;

		String worldname = event.getWhoClicked().getWorld().getName();
		String name = stack.getItemMeta().getDisplayName().replaceAll("\u00A7.", "");
		PetType type = PetType.getTypeByName(name);

		if (type != null)
		{
			// First save the previous pet.
			Pet prevPet = OwnerToPet.get(worldname, this.player.getName());
			if (prevPet != null)
				this.plugin.savePets(new PetSettings(this.player.getName(), prevPet, worldname));

			// then check if there are already saved settings
			Pet pet;
			try {
				pet = this.plugin.loadPetSettings(this.player.getName(), worldname, type, this.stand);
				if (Settings.DEBUG)
					this.plugin.getLogger().info("Loaded existing pet for " + this.player.getName());
			} catch (PlayerFileDoesNotExistException e) {
				// No pre-saved pet
				pet = Pet.createPet(type, this.player.getName(), this.stand);
				if (Settings.DEBUG)
					this.plugin.getLogger().info("No existing config found for " + this.player.getName());
			} catch (PetConfigDoesNotExistException e) {
				// No pre-saved pet
				pet = Pet.createPet(type, this.player.getName(), this.stand);
				if (Settings.DEBUG)
					this.plugin.getLogger().info("No existing type config found for " + this.player.getName());
			}

			if (pet == null)
			{
				ASPetPlugin.error(this.player, "Sorry, but this type of pet is not supported yet!");
				this.player.closeInventory();
				return true;
			}

			// Create pet and whatever
			OwnerToPet.put(worldname, this.player.getName(), pet);
			StandToOwner.put(worldname, this.stand, this.player.getName());

			this.plugin.getLogger().info("Player " + this.player.getName() + " created a new pet: "
					+ type.name + " at " + pet.getLocationString());
			ASPetPlugin.inform(this.player, Lang.get("newPet"));
			pet.stand();
			pet.faceOwner();
			pet.isBusy = true;
			pet.say(pet.getInitialMessage(), new NotBusyAction(pet, null));

			this.player.closeInventory();
			StandToOwner.purge();
			return true;
		}
		else
			return false;
	}

	private void initializeInventory()
	{
		PetType[] types = PetType.values();
		for (PetType type: types)
		{
			String typeName = type.name().toLowerCase();
			// Check if it's enabled on the server at all first, and if player has perm
			if (Settings.typeEnables.get(typeName)
					&& this.player.hasPermission("armorstandpet.type." + typeName))
			{
				ItemStack stack = new ItemStack(type.material, 1);
				ItemMeta meta = stack.getItemMeta();
				meta.setDisplayName("\u00A7a" + type.name);
				meta.setLore(type.lore);

				stack.setItemMeta(meta);

				this.inventory.addItem(stack);
			}
		}
	}

	public Inventory getInventory()
	{
		return this.inventory;
	}
}
