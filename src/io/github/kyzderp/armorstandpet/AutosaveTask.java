/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet;

import org.bukkit.scheduler.BukkitRunnable;

public class AutosaveTask extends BukkitRunnable
{
	@Override
	public void run()
	{
		ASPetPlugin.getInstance().getLogger().info("Auto-saving pets...");
		ASPetPlugin.getInstance().saveAllPets();
	}
}
