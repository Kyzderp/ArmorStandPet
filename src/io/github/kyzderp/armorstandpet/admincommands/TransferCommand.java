/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;

public class TransferCommand extends BaseAdminCommand 
{

	public TransferCommand(ASPetPlugin plugin) 
	{
		super(plugin, "transfer");
		this.description = Lang.get("aspetadmin.transfer.desc");
		this.usage = "/aspetadmin transfer <source> <target> [world]";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (args.length != 3 && args.length != 4)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}
		
		// Find the world
		String worldname = null;
		if (sender.equals(sender.getServer().getConsoleSender()))
		{
			if (args.length != 4)
			{
				this.error(sender, Lang.get("aspetadmin.transfer.console"));
				return;
			}
			worldname = args[3];
		}
		else
		{
			if (args.length == 4)
				worldname = args[3];
			else
			{
				if (sender instanceof LivingEntity)
					worldname = ((LivingEntity) sender).getWorld().getName();
				else if (sender instanceof BlockCommandSender)
					worldname = ((BlockCommandSender) sender).getBlock().getWorld().getName();
			}
		}
		if (worldname == null)
		{
			this.error(sender, Lang.get("aspetadmin.transfer.needWorld"));
			return;
		}
		
		
		String source = args[1];
		Pet pet = OwnerToPet.remove(worldname, source);
		if (pet == null)
		{
			this.error(sender, Lang.get("aspetadmin.transfer.noPet", source));
			return;
		}
		
		String target = args[2];
		pet.setOwner(target);
		OwnerToPet.put(worldname, target, pet);
		StandToOwner.put(worldname, pet.getStand(), target);
		
		ASPetPlugin.inform(sender, Lang.get("aspetadmin.transfer.success",
				new String[] {source, target})); 
	}
}
