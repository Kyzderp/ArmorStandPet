/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;

import org.bukkit.command.CommandSender;

public class ReloadCommand extends BaseAdminCommand 
{

	public ReloadCommand(ASPetPlugin plugin) 
	{
		super(plugin, "reload");
		this.description = Lang.get("aspetadmin.reload.desc");
		this.usage = "/aspetadmin reload";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		this.plugin.reloadConfig();
		this.plugin.getSettings().loadSettings();
		ASPetPlugin.inform(sender, Lang.get("aspetadmin.reload.config"));
		Lang.loadStrings(plugin);
		ASPetPlugin.inform(sender, Lang.get("aspetadmin.reload.lang"));
	}
}
