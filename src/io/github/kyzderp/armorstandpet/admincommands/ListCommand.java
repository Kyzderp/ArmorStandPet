/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ListCommand extends BaseAdminCommand 
{

	public ListCommand(ASPetPlugin plugin) 
	{
		super(plugin, "list");
		this.description = Lang.get("aspetadmin.list.desc");
		this.usage = "/aspetadmin list";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		// Only show for their world
		if (sender instanceof Player || sender instanceof BlockCommandSender)
		{
			String worldname;
			if (sender instanceof Player)
				worldname = ((Player) sender).getWorld().getName();
			else
				worldname = ((BlockCommandSender) sender).getBlock().getWorld().getName();

			if (StandToOwner.getWorld(worldname).isEmpty())
			{
				ASPetPlugin.inform(sender, Lang.get("aspetadmin.list.noPets"));
				return;
			}	

			String result = Lang.get("aspetadmin.list.worldHeader");
			for (String owner: OwnerToPet.getWorld(worldname).keySet())
				result += "\u00A7f" + owner + "\u00A77(" 
						+ OwnerToPet.get(worldname, owner).type.name + ") ";
			ASPetPlugin.inform(sender, result);
		}

		else
		{
			// For console, show all worlds
			int i = 0;
			String[] result = new String[StandToOwner.getWorlds().size()];
			for (String worldname: StandToOwner.getWorlds())
			{
				String inner = Lang.get("aspetadmin.list.allWorldHeader", worldname); 
				if (StandToOwner.getWorld(worldname).isEmpty())
					inner += " \u00A7cNone";
				else
				{
					for (String owner: OwnerToPet.getWorld(worldname).keySet())
						inner += " \u00A7f" + owner + "\u00A77(" 
								+ OwnerToPet.get(worldname, owner).type.name + ")";
				}
				result[i] = inner;
				i++;
			}
			ASPetPlugin.informRaw(sender, result);
		}
	}
}
