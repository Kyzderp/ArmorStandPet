/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;

import org.bukkit.command.CommandSender;

public abstract class BaseAdminCommand 
{
	protected ASPetPlugin plugin;
	protected String description;
	protected String usage;
	protected boolean requiresPerm;
	protected String name;
	
	public BaseAdminCommand(ASPetPlugin plugin, String name)
	{
		this.plugin = plugin;
		this.name = name;
		this.description = "";
		this.usage = "";
		this.requiresPerm = true;
	}
	
	public void execute(CommandSender sender, String[] args)
	{
		if (this.requiresPerm && !sender.hasPermission("armorstandpet.admin." + name))
			this.error(sender, Lang.get("aspetadmin.noPermission"));
		else
			this.run(sender, args);
	}
	
	protected abstract void run(CommandSender sender, String[] args);
	
	protected void error(CommandSender sender, String message)
	{
		ASPetPlugin.error(sender, message);
	}
}
