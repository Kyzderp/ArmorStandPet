/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;

import java.util.Map;

import org.bukkit.command.CommandSender;

public class HelpCommand extends BaseAdminCommand 
{
	private Map<String, BaseAdminCommand> commands;

	public HelpCommand(ASPetPlugin plugin, Map<String, BaseAdminCommand> commands) 
	{
		super(plugin, "help");
		this.commands = commands;
		this.usage = "/aspetadmin help";
		this.description = Lang.get("aspetadmin.help.desc");
		this.requiresPerm = false;
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		String[] toDisplay = new String[this.commands.size() + 1];
		String[] keys = this.commands.keySet().toArray(new String[0]);
		for (int i = 0; i < keys.length; i++)
			toDisplay[i + 1] = this.formatHelp(sender, keys[i]);

		toDisplay[0] = Lang.get("aspetadmin.helpHeader");

		sender.sendMessage(toDisplay);
	}

	private String formatHelp(CommandSender sender, String cmd)
	{
		BaseAdminCommand command = this.commands.get(cmd);
		if (!command.requiresPerm || sender.hasPermission("armorstandpet.admin." + cmd))
		{
			String result = "\u00A7a" + command.usage + " \u00A7f- \u00A77";
			result += command.description;
			return result;
		}
		return "";
	}
}
