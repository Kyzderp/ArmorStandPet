/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Bukkit;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class KillpCommand extends BaseAdminCommand 
{

	public KillpCommand(ASPetPlugin plugin) 
	{
		super(plugin, "killp");
		this.description = Lang.get("aspetadmin.killp.desc");
		this.usage = "/aspetadmin killp <playername> [world]";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		// CAN be run by console
		
		if (args.length != 2 && args.length != 3)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}
		
		// Find the world name
		String worldname = null;
		if (sender.equals(sender.getServer().getConsoleSender()))
		{
			if (args.length != 3)
			{
				this.error(sender, Lang.get("aspetadmin.killp.console"));
				return;
			}
			worldname = args[2];
		}
		else
		{
			if (args.length == 3)
				worldname = args[2];
			else
			{
				if (sender instanceof LivingEntity)
					worldname = ((LivingEntity) sender).getWorld().getName();
				else if (sender instanceof BlockCommandSender)
					worldname = ((BlockCommandSender) sender).getBlock().getWorld().getName();
			}
		}
		if (worldname == null)
		{
			this.error(sender, Lang.get("aspetadmin.killp.needWorld"));
			return;
		}
		
		Pet pet = OwnerToPet.remove(worldname, args[1]);
		
		if (pet == null)
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.killp.noPet", args[1]));
			return;
		}
		
		pet.delete(true, true);
		
		ASPetPlugin.inform(sender, Lang.get("aspetadmin.killp.success", args[1]));
		
		// If the player is online, tell them too
		Player p = Bukkit.getPlayer(args[1]);
		if (p != null && p.isOnline())
			ASPetPlugin.inform(p, Lang.get("aspetadmin.killp.messageToOwner"));
	}
}
