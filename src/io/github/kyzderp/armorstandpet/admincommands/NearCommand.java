/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.ASPetUtils;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;

public class NearCommand extends BaseAdminCommand 
{

	public NearCommand(ASPetPlugin plugin) 
	{
		super(plugin, "near");
		this.description = Lang.get("aspetadmin.near.desc");
		this.usage = "/aspetadmin near [radius]";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		// Cannot be run by console
		if (!(sender instanceof Player) && !(sender instanceof BlockCommandSender))
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.near.ingame"));
			return;
		}
		
		Location loc = null;
		if (sender instanceof Player)
			loc = ((Player) sender).getLocation();
		if (sender instanceof BlockCommandSender)
			loc = ((BlockCommandSender) sender).getBlock().getLocation();
		
		// Default radius is 32, if not specified
		double radius = 32.0;
		try {
			if (args.length == 2)
				radius = Double.parseDouble(args[1]);
			if (radius < 0)
			{
				ASPetPlugin.error(sender, Lang.get("aspetadmin.near.negativeRadius"));
				return;
			}
		} catch (NumberFormatException e) {
			ASPetPlugin.error(sender, Lang.get("aspetadmin.near.radiusNumber"));
		}
		
		String worldname = loc.getWorld().getName();

		// Get all the stands within the area first
		Map<String, Double> stands = new HashMap<String, Double>();
		for (ArmorStand stand: StandToOwner.getWorld(worldname).keySet())
		{
			double distSq = stand.getLocation().distanceSquared(loc);
			if (distSq <= radius * radius)
			{
				// This stand is close enough to be included in the display
				String owner = StandToOwner.get(worldname, stand);
				stands.put(owner, Math.sqrt(distSq));
			}
		}
		
		if (stands.isEmpty())
		{
			ASPetPlugin.inform(sender, Lang.get("aspetadmin.near.noPets", radius));
			return;
		}
		
		// Sort and display from closest to furthest
		Map<String, Double> sorted = ASPetUtils.sortValueAscending(stands);
		String result = "";
		for (String owner: sorted.keySet())
			result += "\u00A7f" + owner + "\u00A77(" + sorted.get(owner).intValue() + ") ";
		
		result = Lang.get("aspetadmin.near.result",
				new Object[] {stands.size(), radius, result});
		ASPetPlugin.inform(sender, result);
	}
}
