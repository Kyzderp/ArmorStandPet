/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.PetHolder;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;

import org.bukkit.command.CommandSender;

public class LoadCommand extends BaseAdminCommand 
{

	public LoadCommand(ASPetPlugin plugin) 
	{
		super(plugin, "load");
		this.description = Lang.get("aspetadmin.load.desc");
		this.usage = "/aspetadmin load <kill|preserve>";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (args.length == 2)
		{
			if ("kill".startsWith(args[1]))
			{
				StandToOwner.killall();
				new PetHolder();
				this.plugin.loadPets(false);
				ASPetPlugin.inform(sender, Lang.get("aspetadmin.load.kill"));
				return;
			}
			else if ("preserve".startsWith(args[1]))
			{
				new PetHolder();
				this.plugin.loadPets(true);
				ASPetPlugin.inform(sender, Lang.get("aspetadmin.load.preserve"));
				return;
			}
			else
			{
				ASPetPlugin.error(sender, Lang.get("aspetadmin.load.usage"));
				return;
			}
		}
		else
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.load.usage"));
			return;
		}
	}
}
