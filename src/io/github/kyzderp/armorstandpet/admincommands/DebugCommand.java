/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.struct.UnloadedPets;

import org.bukkit.command.CommandSender;

public class DebugCommand extends BaseAdminCommand 
{

	public DebugCommand(ASPetPlugin plugin) 
	{
		super(plugin, "debug");
		this.description = Lang.get("aspetadmin.debug.desc");
		this.usage = "/aspetadmin debug";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		UnloadedPets.dump();
		StandToOwner.dump();
		OwnerToPet.dump();
		
		ASPetPlugin.inform(sender, Lang.get("aspetadmin.debug.success"));
	}
}
