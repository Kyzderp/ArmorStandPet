/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;

import org.bukkit.command.CommandSender;

public class SaveCommand extends BaseAdminCommand 
{

	public SaveCommand(ASPetPlugin plugin) 
	{
		super(plugin, "save");
		this.description = Lang.get("aspetadmin.save.desc");
		this.usage = "/aspetadmin save";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		this.plugin.saveAllPets();
		ASPetPlugin.inform(sender, Lang.get("aspetadmin.save.data"));
	}
}
