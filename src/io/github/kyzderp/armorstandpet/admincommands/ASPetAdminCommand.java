/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ASPetAdminCommand implements CommandExecutor
{
	private ASPetPlugin plugin;
	private Map<String, BaseAdminCommand> commands;

	public ASPetAdminCommand(ASPetPlugin plugin)
	{
		this.plugin = plugin;
		this.commands = new LinkedHashMap<String, BaseAdminCommand>();
		// near|killp|killr|unbindp|unbindr|list|tp
		this.commands.put("list", new ListCommand(this.plugin));
		this.commands.put("near", new NearCommand(this.plugin));
		this.commands.put("tp", new TpCommand(this.plugin));
		this.commands.put("tphere", new TphereCommand(this.plugin));
		this.commands.put("killp", new KillpCommand(this.plugin));
		this.commands.put("unbindp", new UnbindpCommand(this.plugin));
		this.commands.put("killr", new KillrCommand(this.plugin));
		this.commands.put("unbindr", new UnbindrCommand(this.plugin));
		this.commands.put("transfer", new TransferCommand(this.plugin));
		
		this.commands.put("reload", new ReloadCommand(this.plugin));
		this.commands.put("load", new LoadCommand(this.plugin));
		this.commands.put("save", new SaveCommand(this.plugin));
		this.commands.put("debug", new DebugCommand(this.plugin));
		
		this.commands.put("help", new HelpCommand(this.plugin, this.commands));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
//		if (!cmd.getName().equals("aspetadmin"))
//			return false;

		// No arguments
		if (args.length == 0)
		{
			ASPetPlugin.inform(sender, "\u00A7fArmorStandPet \u00A77by \u00A7fKyzeragon. "
					+ Lang.get("aspetadmin.seeHelp"));
			return true;
		}

		// See if it's a valid command
		BaseAdminCommand command = this.commands.get(args[0].toLowerCase());
		if (command == null)
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.invalidSyntax"));
			return true;
		}

		// Run the actual command
		command.execute(sender, args);

		return true;
	}

}
