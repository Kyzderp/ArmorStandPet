/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.ASPetUtils;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;

public class UnbindrCommand extends BaseAdminCommand 
{

	public UnbindrCommand(ASPetPlugin plugin) 
	{
		super(plugin, "unbindr");
		this.description = Lang.get("aspetadmin.unbindr.desc");
		this.usage = "/aspetadmin unbindr <radius>";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		// Cannot be run by console
		if (!(sender instanceof Player) && !(sender instanceof BlockCommandSender))
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.unbindr.ingame"));
			return;
		}
		Location loc = null;
		if (sender instanceof Player)
			loc = ((Player) sender).getLocation();
		if (sender instanceof BlockCommandSender)
			loc = ((BlockCommandSender) sender).getBlock().getLocation();
		
		if (args.length != 2)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}
		
		double radius = 0;
		try {
			radius = Double.parseDouble(args[1]);
			if (radius < 0)
			{
				ASPetPlugin.error(sender, Lang.get("aspetadmin.unbindr.negativeRadius"));
				return;
			}
		} catch (NumberFormatException e) {
			ASPetPlugin.error(sender, Lang.get("aspetadmin.unbindr.radiusNumber"));
			return;
		}
		
		String worldname = loc.getWorld().getName();

		// Get all the stands within the area first
		Map<String, Double> stands = new HashMap<String, Double>();
		for (ArmorStand stand: StandToOwner.getWorld(worldname).keySet())
		{
			double distSq = stand.getLocation().distanceSquared(loc);
			if (distSq <= radius * radius)
			{
				// This stand is close enough to be included in the display
				String owner = StandToOwner.get(worldname, stand);
				stands.put(owner, Math.sqrt(distSq));
			}
		}
		
		// Sort and display and kill from closest to furthest
		Map<String, Double> sorted = ASPetUtils.sortValueAscending(stands);
		String result = "";
		for (String owner: sorted.keySet())
		{
			result += "\u00A7f" + owner + "\u00A77(" + sorted.get(owner).intValue() + ") ";
			Pet pet = OwnerToPet.remove(worldname, owner);
			pet.delete(true, false);
			
			// If the player is online, tell them too
			Player p = Bukkit.getPlayer(owner);
			if (p != null && p.isOnline())
				ASPetPlugin.inform(p, Lang.get("aspetadmin.unbindr.messageToOwner"));
		}
		
		result = Lang.get("aspetadmin.unbindr.result",
				new Object[] {stands.size(), radius, result});
		ASPetPlugin.inform(sender, result);
	}
}
