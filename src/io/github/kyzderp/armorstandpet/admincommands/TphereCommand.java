/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.admincommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TphereCommand extends BaseAdminCommand 
{

	public TphereCommand(ASPetPlugin plugin) 
	{
		super(plugin, "tphere");
		this.description = Lang.get("aspetadmin.tphere.desc");
		this.usage = "/aspetadmin tphere <playername>";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		// Needs to be a player
		if (!(sender instanceof Player))
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.tphere.ingame"));
			return;
		}
		
		if (args.length < 2)
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.tphere.usage"));
			return;
		}
		
		// Doesn't actually have a pet
		Pet pet = OwnerToPet.get(((Player)sender).getWorld().getName(), args[1]);
		if (pet == null)
		{
			ASPetPlugin.error(sender, Lang.get("aspetadmin.tphere.noPet", args[1])); 
			return;
		}
		
		// Else tp to them
		pet.getStand().teleport((Player)sender);
		ASPetPlugin.inform(sender, Lang.get("aspetadmin.tphere.teleporting", args[1])); 
	}
}
