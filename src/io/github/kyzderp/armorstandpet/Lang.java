/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Lang 
{
	private static Map<String, String> strings;

	//////////////////////// LOAD ///////////////////////
	public static void loadStrings(JavaPlugin plugin)
	{
		YamlConfiguration config;
		File file = new File(plugin.getDataFolder(), "lang.yml");
		if (!file.exists())
		{
			config = YamlConfiguration.loadConfiguration(new InputStreamReader(plugin.getResource("lang.yml")));
			try {
				config.save(file);
			} catch (IOException e) {
				plugin.getLogger().warning("Unable to save lang.yml!");
			}
		}
		else
			config = YamlConfiguration.loadConfiguration(file);

		strings = new HashMap<String, String>();

		Set<String> keys = config.getKeys(true);
		for (String key: keys)
		{
			String line = config.getString(key);
			if (line != null)
				strings.put(key, line);
			else
				plugin.getLogger().warning("Unable to get language string for: " + key);
		}

		plugin.getLogger().info("Loaded " + strings.size() + " strings.");
	}

	/**
	 * Get the string, or return error if unspecified
	 * @param key
	 * @return
	 */
	public static String get(String key)
	{
		String result = strings.get(key);
		if (result == null)
			return "Message unspecified: " + key 
					+ ". Contact your server administrator to report this issue.";
		else
			return result.replaceAll("&", "\u00A7");
	}
	
	public static String get(String key, Object arg)
	{
		return Lang.get(key, new Object[] {arg});
	}
	
	public static String get(String key, Object[] args)
	{
		String result = strings.get(key);
		if (result == null)
			return "Message unspecified: " + key 
					+ ". Contact your server administrator to report this issue.";
		else
		{
			for (int i = 0; i < args.length; i++)
				result = result.replaceAll("\\{" + i + "\\}", args[i].toString());
			return result.replaceAll("&", "\u00A7");
		}
	}
}
