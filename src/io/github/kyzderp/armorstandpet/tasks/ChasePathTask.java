/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.tasks;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.ai.Path;
import io.github.kyzderp.armorstandpet.ai.algorithms.PathFinder;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ChasePathTask extends ASPetTask
{
	private Path path;
	private double distance;
	private int iters;

	private Player player;
	private Double playerDistance;
	private boolean imperative;

	public ChasePathTask(Pet pet, Path path, ASPetAction action, 
			double distance, int iters, Player player, Double playerDistance, boolean imperative)
	{
		super(pet, action);
		this.path = path;
		this.distance = distance;
		this.iters = iters;
		this.player = player;
		this.playerDistance = playerDistance;
		this.imperative = imperative;
	}

	public ChasePathTask(Pet pet, Path path, List<ASPetAction> action, 
			double distance, int iters, Player player, Double playerDistance, boolean imperative)
	{
		super(pet, action);
		this.path = path;
		this.distance = distance;
		this.iters = iters;
		this.player = player;
		this.playerDistance = playerDistance;
		this.imperative = imperative;
	}

	public ChasePathTask(Pet pet, Path path, ASPetAction action, 
			double distance, int iters, Player player, Double playerDistance)
	{
		super(pet, action);
		this.path = path;
		this.distance = distance;
		this.iters = iters;
		this.player = player;
		this.playerDistance = playerDistance;
	}

	public ChasePathTask(Pet pet, Path path, List<ASPetAction> action, 
			double distance, int iters, Player player, Double playerDistance)
	{
		super(pet, action);
		this.path = path;
		this.distance = distance;
		this.iters = iters;
		this.player = player;
		this.playerDistance = playerDistance;
	}

	public ChasePathTask(Pet pet, Path path, ASPetAction action, double distance, int iters)
	{
		super(pet, action);
		this.path = path;
		this.distance = distance;
		this.iters = iters;
	}

	public ChasePathTask(Pet pet, Path path, List<ASPetAction> action, double distance, int iters)
	{
		super(pet, action);
		this.path = path;
		this.distance = distance;
		this.iters = iters;
	}

	@Override
	public void run() 
	{
		// In case player dc's while it's going or stand gets killed
		if (this.pet.getStand().isDead())
		{
			this.pet.walkFlat();
			this.done();
			return;
		}

		// Path is completely null
		if (this.path == null)
		{
			PathFinder pathFinder = PathFinder.getPathfinder(pet.getStand().getLocation(), 
					this.path.end, Settings.AI);
			Path path = pathFinder.findPath();
			if (Settings.DEBUG)
				ASPetPlugin.getInstance().getLogger().info(path.dump());

			(new ChasePathTask(this.pet, path, this.callback, 
					this.distance, 0, this.player, this.playerDistance)).run();
			return;
		}

		// already same location
		if (this.path.isSame)
		{
			// Player's location is above, but not quite close enough
			// to reach. Only teleport if imperative.
			if (this.player != null && this.imperative)
			{
				this.pet.teleportToFrontOf(this.player.getLocation(), this.playerDistance);
				this.pet.facePlayer(this.player);
			}
			this.pet.walkFlat();
			this.done();
			return;
		}

		// Cannot find a path, so just teleport
		if (this.path.nodes == null)
		{
			if (this.player != null)
			{
				this.pet.teleportToFrontOf(this.player.getLocation(), this.playerDistance);
				this.pet.facePlayer(this.player);
			}
			else
			{
				this.pet.teleportTo(this.path.end);
			}
			this.pet.walkFlat();
			this.done();
			return;
		}

		// In case of different world
		if (!this.pet.getStand().getWorld().equals(this.path.end.getWorld()))
		{
			this.done();
			return;
		}

		// Waaaaay too far away, teleport!
		if (this.player != null && this.pet.distanceSq(this.player) > Settings.teleportRange)
		{
			this.pet.teleportToFrontOf(this.player.getLocation(), this.playerDistance);
			this.pet.facePlayer(this.player);

			this.pet.walkFlat();
			this.done();
			return;
		}

		if (this.pet.distanceSq(this.path.end) > Settings.teleportRange)
		{
			this.pet.teleportTo(this.path.end);

			this.pet.walkFlat();
			this.done();
			return;
		}

		Location currLoc = this.path.nodes.get(0).getLocation(this.path.end.getWorld());

		// Been trying too long to get there, just teleport. Probably stuck. 
		if (this.iters > 20)
		{
			if (Settings.DEBUG)
				ASPetPlugin.getInstance().getLogger().warning("Max iterations reached, teleporting pet.");
			this.pet.getStand().teleport(currLoc);
		}

		// Close enough, pop off the first
		if (this.pet.distanceSq(currLoc) < this.distance)
		{
			this.path.nodes.remove(0);
			this.iters = 0;

			// Done with everything
			if (this.path.nodes.isEmpty())
			{
				// Player's location is above, but not quite close enough
				// to reach. Only teleport if imperative.
				if (this.player != null && this.imperative)
				{
					if (this.player.getLocation().getBlockX() == this.pet.getStand().getLocation().getBlockX()
							&& this.player.getLocation().getBlockZ() == this.pet.getStand().getLocation().getBlockZ())
					{
						this.pet.teleportToFrontOf(this.player.getLocation(), this.playerDistance);
						this.pet.facePlayer(this.player);
						this.pet.walkFlat();
						this.done();
						return;
					}
				}

				// Make sure we've caught up to the player
				else if (this.player != null)
				{
					// Not caught up
					if (this.pet.distanceSq(this.player) > this.playerDistance)
					{
						PathFinder pathFinder = PathFinder.getPathfinder(pet.getStand().getLocation(), 
								this.player.getLocation(), Settings.AI);
						Path path = pathFinder.findPath();
						if (Settings.DEBUG)
							ASPetPlugin.getInstance().getLogger().info(path.dump());

						(new ChasePathTask(this.pet, path, this.callback, 
								this.distance, 0, this.player, this.playerDistance)).run();
						return;
					}
				}

				this.pet.walkFlat();
				this.done();
				return;
			}

			// If it's set to continuous, we need to recalculate the path after every node
			if (Settings.continuous)
			{
				// If chasing player, use player's location
				if (this.player != null)
				{
					PathFinder pathFinder = PathFinder.getPathfinder(pet.getStand().getLocation(), 
							this.player.getLocation(), Settings.AI);
					this.path = pathFinder.findPath();
				}
				else
				{
					PathFinder pathFinder = PathFinder.getPathfinder(pet.getStand().getLocation(), 
							this.path.end, Settings.AI);
					this.path = pathFinder.findPath();
				}
				if (Settings.DEBUG)
					ASPetPlugin.getInstance().getLogger().info(this.path.dump());
				(new ChasePathTask(this.pet, this.path, 
						this.callback, this.distance, 0, this.player, this.playerDistance))
						.runTaskLater(ASPetPlugin.getInstance(), 3);
				return;
			}

			// Keep going
			currLoc = this.path.nodes.get(0).getLocation(this.path.end.getWorld());
		}

		// Else, take a step in the direction
		this.pet.faceLoc(currLoc);
		this.pet.animateWalk();
		this.pet.takeStep();

		(new ChasePathTask(this.pet, this.path, 
				this.callback, this.distance, this.iters + 1, this.player, this.playerDistance))
				.runTaskLater(ASPetPlugin.getInstance(), 3);
	}

}
