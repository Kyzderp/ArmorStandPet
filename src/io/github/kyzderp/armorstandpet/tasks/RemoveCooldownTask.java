/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.tasks;

import io.github.kyzderp.armorstandpet.types.DoormanPet;

import org.bukkit.scheduler.BukkitRunnable;

public class RemoveCooldownTask extends BukkitRunnable 
{
	private DoormanPet pet;
	private String playername;
	
	public RemoveCooldownTask(DoormanPet pet, String playername)
	{
		this.pet = pet;
		this.playername = playername;
	}

	@Override
	public void run() 
	{
		this.pet.removeCooldown(this.playername);
	}
}
