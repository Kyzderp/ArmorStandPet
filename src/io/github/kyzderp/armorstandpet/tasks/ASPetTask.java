/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.tasks;

import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.scheduler.BukkitRunnable;

public abstract class ASPetTask extends BukkitRunnable
{
	protected Pet pet;
	protected List<ASPetAction> callback;

	public ASPetTask(Pet pet, ASPetAction callback)
	{
		this.pet = pet;
		if (callback == null)
			this.callback = null;
		else
		{
			this.callback = new ArrayList<ASPetAction>();
			this.callback.add(callback);
		}
	}

	public ASPetTask(Pet pet, List<ASPetAction> callback)
	{
		this.pet = pet;
		this.callback = callback;
	}
	
	/**
	 * Call this function when done with execution to do callbacks
	 */
	protected void done()
	{
		if (this.callback != null)
		{
			for (ASPetAction action: this.callback)
				action.execute();
		}
	}
}
