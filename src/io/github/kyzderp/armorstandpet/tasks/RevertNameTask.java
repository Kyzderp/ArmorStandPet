/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.tasks;

import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

public class RevertNameTask extends ASPetTask
{
	public RevertNameTask(Pet pet, ASPetAction callback) 
	{
		super(pet, callback);
	}

	public RevertNameTask(Pet pet, List<ASPetAction> callback) 
	{
		super(pet, callback);
	}

	@Override
	public void run() 
	{
		if (this.pet.getName().isEmpty())
		{
			this.pet.getStand().setCustomName("");
			this.pet.getStand().setCustomNameVisible(false);
		}
		else
		{
			this.pet.getStand().setCustomName(this.pet.getName());
			this.pet.getStand().setCustomNameVisible(true);
		}
		this.done();
	}
}
