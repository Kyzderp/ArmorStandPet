/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.tasks;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

import org.bukkit.Effect;
import org.bukkit.entity.Player;

public class WalkPlayerTask extends ASPetTask
{
	private Player dest;
	private double distance;

	public WalkPlayerTask(Pet pet, Player dest, ASPetAction action, double distance)
	{
		super(pet, action);
		this.dest = dest;
		this.distance = distance;
	}
	
	public WalkPlayerTask(Pet pet, Player dest, List<ASPetAction> action, double distance)
	{
		super(pet, action);
		this.dest = dest;
		this.distance = distance;
	}

	@Override
	public void run() 
	{
		// In case player dc's while it's going or stand gets killed
		if (!this.dest.isOnline() || this.pet.getStand().isDead())
		{
			this.done();
			return;
		}
		
		// In case of different world
		if (!this.pet.getStand().getWorld().equals(this.dest.getWorld()))
		{
			this.done();
			return;
		}
		
		// Close enough!
		if (this.pet.distanceSq(this.dest) < this.distance)
		{
			this.pet.walkFlat();
			this.pet.facePlayer(this.dest);
			this.done();
			return;
		}

		// Waaaaay too far away, teleport!
		if (this.pet.distanceSq(this.dest) > Settings.teleportRange)
		{
			this.pet.getStand().getWorld().playEffect(this.pet.getStand().getLocation(), 
					Effect.ENDER_SIGNAL, 0);
			this.pet.getStand().teleport(this.dest);
			this.pet.getStand().getWorld().playEffect(this.pet.getStand().getLocation(), 
					Effect.ENDER_SIGNAL, 0);
			this.pet.walkFlat();
			this.done();
			return;
		}

		// Else, take a step in the direction
		this.pet.facePlayer(this.dest);
		this.pet.animateWalk();
		this.pet.takeStep();

		(new WalkPlayerTask(this.pet, this.dest, this.callback, this.distance))
		.runTaskLater(ASPetPlugin.getInstance(), 3);
	}

}
