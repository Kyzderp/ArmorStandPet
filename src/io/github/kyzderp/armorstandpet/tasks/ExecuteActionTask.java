/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.tasks;

import io.github.kyzderp.armorstandpet.actions.ASPetAction;

import org.bukkit.scheduler.BukkitRunnable;

public class ExecuteActionTask extends BukkitRunnable 
{
	private ASPetAction action;
	
	public ExecuteActionTask(ASPetAction action)
	{
		this.action = action;
	}
	
	public void run()
	{
		this.action.execute();
	}
}
