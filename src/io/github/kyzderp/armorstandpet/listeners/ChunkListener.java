/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.listeners;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.ASPetUtils;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.struct.UnloadedPets;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

public class ChunkListener implements Listener
{
	private ASPetPlugin plugin;

	public ChunkListener(ASPetPlugin plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority=EventPriority.LOWEST)
	public void onChunkUnload(ChunkUnloadEvent event)
	{
		String worldname = event.getWorld().getName();
		String chunkString = event.getChunk().getX() + " " + event.getChunk().getZ();
		Entity[] entities = event.getChunk().getEntities();
		for (Entity entity: entities)
		{
			if (entity instanceof ArmorStand)
			{
				ArmorStand stand = (ArmorStand)entity;
				
				String owner = StandToOwner.get(worldname, stand); 
				if (owner != null)
				{
					UnloadedPets.putStand(worldname, chunkString, stand);
					stand.remove();
					if (Settings.DEBUG)
						this.plugin.getLogger().info("Remove " + owner 
								+ "'s armor stand due to unload " + event.getChunk());
				}
			}
		}
	}

	@EventHandler
	public void onChunkLoad(ChunkLoadEvent event)
	{
		String worldname = event.getWorld().getName();
//		if (Settings.DEBUG)
//			ASPetPlugin.getInstance().getLogger().info("Chunk " 
//					+ event.getChunk().getX() + " " + event.getChunk().getZ());
		/*if (event.getChunk().getX() == -15 && event.getChunk().getZ() == 7)
		{
			ASPetPlugin.getInstance().getLogger().info("IT'S MY CHUNK!");
			UnloadedPets.dump();
			StandToOwner.dump();
			List<ArmorStand> z = UnloadedPets.get(worldname, 
					event.getChunk().getX() + " " + event.getChunk().getZ());
			if (z == null)
				ASPetPlugin.getInstance().getLogger().info("wtf why is it null");
			else
			{
				ASPetPlugin.getInstance().getLogger().info("Size: " + z.size());
			}
		}*/
		
		List<ArmorStand> pets = UnloadedPets.remove(worldname, 
				event.getChunk().getX() + " " + event.getChunk().getZ());
		if (pets == null || pets.isEmpty())
			return;

		for (ArmorStand stand: pets)
		{
			String owner = StandToOwner.get(worldname, stand);
			if (owner == null)
			{
				ASPetPlugin.getInstance().getLogger().info("Owner is null");
				continue;
			}

			ArmorStand newStand = ASPetUtils.respawnStand(stand);
			StandToOwner.remove(worldname, stand);
			StandToOwner.put(worldname, newStand, owner);

			Pet pet = OwnerToPet.get(worldname, owner); 
			pet.setStand(newStand);
		/*	if (pet != null && pet instanceof DoormanPet)
			{
				Doormen.remove(worldname, stand);
				Doormen.put(worldname, newStand, (DoormanPet) pet);
			}*/

			stand.remove();
			
			if (Settings.DEBUG)
				this.plugin.getLogger().info("Respawned " + owner + "'s pet due to chunk load"
						+ " on chunk " + event.getChunk().getX() + " " + event.getChunk().getZ());

		}
	}
}