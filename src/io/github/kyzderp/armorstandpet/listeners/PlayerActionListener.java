/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.listeners;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.ASPetUtils;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.actions.WalkAction;
import io.github.kyzderp.armorstandpet.gui.ChooseTypeGUI;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.types.DoormanPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class PlayerActionListener implements Listener
{
	private ASPetPlugin plugin;

	public PlayerActionListener(ASPetPlugin plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();
		String worldname = event.getTo().getWorld().getName();

		for (Pet pet: OwnerToPet.getWorld(worldname).values())
		{
			// Within doorman's range
			if (pet instanceof DoormanPet)
			{
				DoormanPet doorman = (DoormanPet)pet;
				if (!doorman.isBusy 
						&& doorman.distanceSq(event.getTo()) <= doorman.getRange() * doorman.getRange())
				{
					doorman.greet(player);
				}
			}
		}

		Pet pet = OwnerToPet.get(worldname, player.getName());

		// Player doesn't have a pet
		if (pet == null)
			return;

		// Make pet follow
		if (pet.isMobile && !pet.isBusy && !pet.isSitting 
				&& pet.distanceSq(player) > Settings.followRange)
		{
			// Check if already below
			if (pet.getStand().getLocation().getBlockX() == player.getLocation().getBlockX()
					&& pet.getStand().getLocation().getBlockZ() == player.getLocation().getBlockZ())
			{
				// TODO: flying
				return;
			}
			pet.isBusy = true;
			pet.displayMessage(Lang.get("catchUp", player.getName()), null);
			ASPetAction afterAnnouncing = new NotBusyAction(pet, null);
			ASPetAction afterArriving = new SayAction(pet, afterAnnouncing, pet.getAnnounce(player.getName()));
			ASPetAction startWalking = WalkAction.chase(pet, afterArriving, player, 2, false);
			startWalking.execute();
		}
	}

	/**
	 * Disallow damaging pets with left click
	 * @param event
	 */
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerDamageArmorStand(EntityDamageByEntityEvent event)
	{
		if (event.getEntity() instanceof ArmorStand
				&& StandToOwner.containsStand(event.getEntity().getWorld().getName(), 
						(ArmorStand) event.getEntity()))
		{
			ASPetPlugin.error(event.getDamager(), Lang.get("cannotDamage"));
			event.setCancelled(true);
		}
	}

	/**
	 * Handle right clicks on armor stands
	 * @param event
	 */
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.HIGH, ignoreCancelled=true)
	public void onPlayerRightClickStand(PlayerInteractAtEntityEvent event)
	{
		if (event.isCancelled())
			return;
		String worldname = event.getRightClicked().getWorld().getName();

		if (event.getRightClicked() instanceof ArmorStand)
		{
			// Trying to right click someone else's 
			String owner = StandToOwner.get(worldname, (ArmorStand) event.getRightClicked());
			if (owner != null && !owner.equals(event.getPlayer().getName()))
			{
				ASPetPlugin.error(event.getPlayer(), Lang.get("cannotEdit"));
				event.setCancelled(true);
				return;
			}

			// Crouching + right click with empty hand
			if ((event.getPlayer().getItemInHand() == null 
					|| event.getPlayer().getItemInHand().getType() == Material.AIR)
					&& event.getPlayer().isSneaking()
					&& event.getPlayer().hasPermission("armorstandpet.use"))
			{
				// This is already the player's pet, let them change type
				if (owner != null)
				{
					ChooseTypeGUI gui = new ChooseTypeGUI(this.plugin, event.getPlayer(), 
							(ArmorStand)event.getRightClicked());
					String name = event.getPlayer().getName();
					ASPetPlugin.getInstance().getLogger().info("Putting GUI for " + name);
					this.plugin.getGuiListener().getPlayerToGUI().put(name, gui);
					event.getPlayer().openInventory(gui.getInventory());

					event.setCancelled(true);
					return;
				}


				if (!Settings.crossWorld
						&& OwnerToPet.containsOwner(worldname, event.getPlayer().getName()))
				{
					ASPetPlugin.error(event.getPlayer(), Lang.get("alreadyHavePetWorld"));
					return;
				}
				else if (Settings.crossWorld
						&& OwnerToPet.containsOwner(event.getPlayer().getName()))
				{
					// Cross-world, so only allowed to have 1 total
					ASPetPlugin.error(event.getPlayer(), Lang.get("alreadyHavePet"));
					return;
				}

				// TODO: also do a purge when someone tries to get a new pet?
				ChooseTypeGUI gui = new ChooseTypeGUI(this.plugin, event.getPlayer(), 
						(ArmorStand)event.getRightClicked());
				String name = event.getPlayer().getName();
				ASPetPlugin.getInstance().getLogger().info("Putting GUI for " + name);
				this.plugin.getGuiListener().getPlayerToGUI().put(name, gui);

				event.getPlayer().openInventory(gui.getInventory());

				event.setCancelled(true);
			}
		}
	}

	/**
	 * When a player teleports, the pet should teleport too
	 * @param event
	 */
	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event)
	{
		Player player = event.getPlayer();
		String worldname = event.getTo().getWorld().getName();

		Pet pet = OwnerToPet.get(worldname, player.getName());

		// Player doesn't have a pet
		if (pet == null || !pet.isMobile || pet.isSitting)
			return;

		if (pet.getStand().isDead())
		{
			// Dafuq, why is it dead?
			StandToOwner.remove(worldname, pet.getStand());
			ArmorStand newstand = ASPetUtils.respawnStand(pet.getStand());
			StandToOwner.put(worldname, newstand, pet.getOwner());
			pet.setStand(newstand);
		}

		pet.teleportTo(event.getTo());
	}

	/**
	 * Pets should take no damage
	 * @param event
	 */
	@EventHandler(priority=EventPriority.HIGH)
	public void onArmorStandDamaged(EntityDamageEvent event)
	{
		if (event.getEntity() instanceof ArmorStand
				&& StandToOwner.containsStand(event.getEntity().getWorld().getName(), 
						(ArmorStand) event.getEntity()))
		{
			event.setCancelled(true);
		}
	}
}