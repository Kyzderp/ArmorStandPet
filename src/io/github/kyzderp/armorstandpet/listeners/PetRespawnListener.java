/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.listeners;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.ASPetUtils;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.PetHolder;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.struct.UnloadedPets;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PetRespawnListener implements Listener
{
	private ASPetPlugin plugin;

	public PetRespawnListener(ASPetPlugin plugin)
	{
		this.plugin = plugin;
	}

	/**
	 * Upon world change, teleport pet if crossworld enabled
	 * otherwise hide the pet
	 * @param event
	 */
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerChangeWorld(PlayerChangedWorldEvent event)
	{
		// First part, they are leaving this world
		Pet pet = OwnerToPet.get(event.getFrom().getName(), 
				event.getPlayer().getName());

		// Different world, must teleport the pet to the player if cross-world
		if (pet != null && pet.isMobile && Settings.crossWorld)
		{
			// TODO: deal with sitting pets eventually
			pet.stand();

			pet.getStand().teleport(event.getPlayer());
			// Now need to move it in all the maps
			PetHolder.transferWorld(event.getFrom().getName(), 
					event.getPlayer().getWorld().getName(),
					event.getPlayer().getName());
		}
		else if (pet == null)
		{
			this.spawnPet(event.getPlayer().getWorld().getName(), 
					event.getPlayer().getName());
		}
		else
		{
			// Pets are not cross-world, equivalent to player logging out
			if (Settings.hideLogout)
			{
				// Hide the pet 
				if (pet.isMobile)
				{
					// Keep track of hidden pets
					UnloadedPets.hideStand(event.getFrom().getName(), pet.getStand());
					// Remove the armor stand
					pet.getStand().remove();

					// Now get the new pet that's in the new world
					this.spawnPet(event.getPlayer().getWorld().getName(), 
							event.getPlayer().getName());
				}
			}
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		String worldname = event.getPlayer().getWorld().getName();
		this.hidePet(worldname, event.getPlayer().getName());
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		String worldname = event.getPlayer().getWorld().getName();
		this.spawnPet(worldname, event.getPlayer().getName());
	}

	/**
	 * Respawns the pet in a world and fixes the maps
	 * @param worldname
	 * @param owner
	 */
	private void spawnPet(String worldname, String owner)
	{
		Pet pet = OwnerToPet.get(worldname, owner);
		if (pet == null || !pet.isMobile)
		{
			return;
		}
		if (pet.getStand().isDead())
		{
			StandToOwner.remove(worldname, pet.getStand());
			ArmorStand newstand = ASPetUtils.respawnStand(pet.getStand());
			/*Doormen.remove(worldname, pet.getStand());
			if (pet instanceof DoormanPet)
				Doormen.put(worldname, newstand, (DoormanPet)pet);*/
			StandToOwner.put(worldname, newstand, owner);
			pet.setStand(newstand);
			if (Settings.DEBUG)
				this.plugin.getLogger().info("Restored pet for " + owner);
		}
	}

	/**
	 * Hide the pet and fix the maps
	 * @param worldname
	 * @param owner
	 */
	private void hidePet(String worldname, String owner)
	{
		Pet pet = OwnerToPet.get(worldname, owner);
		// Pets are not cross-world, equivalent to player logging out
		if (pet != null && Settings.hideLogout)
		{
			// Hide the pet 
			if (pet.isMobile)
			{
				// Keep track of hidden pets
				UnloadedPets.hideStand(worldname, pet.getStand());
				// Remove the armor stand
				pet.getStand().remove();
			}
		}
	}
}