/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.struct;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.ArmorStand;

public class StandToOwner 
{
	private static Map<String, Map<ArmorStand, String>> map;

	public StandToOwner()
	{
		map = new HashMap<String, Map<ArmorStand, String>>();
	}

	public static void putWorld(String worldname)
	{
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<ArmorStand, String>());
	}

	public static Map<ArmorStand, String> getWorld(String worldname)
	{
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<ArmorStand, String>());
		return map.get(worldname.toLowerCase());
	}

	public static void put(String worldname, ArmorStand stand, String owner)
	{
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<ArmorStand, String>());

		map.get(worldname.toLowerCase()).put(stand, owner);
	}

	public static String get(String worldname, ArmorStand stand)
	{
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<ArmorStand, String>());
		return map.get(worldname.toLowerCase()).get(stand);
	}

	public static boolean containsStand(String worldname, ArmorStand stand)
	{
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<ArmorStand, String>());
		return map.get(worldname.toLowerCase()).containsKey(stand);
	}

	public static boolean containsWorld(String worldname)
	{
		return map.containsKey(worldname.toLowerCase());
	}

	public static String remove(String worldname, ArmorStand stand)
	{
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<ArmorStand, String>());
		return map.get(worldname.toLowerCase()).remove(stand);
	}

	public static void dump()
	{
		ASPetPlugin.getInstance().getLogger().info("StandToOwner Dump:");
		for (String worldname: map.keySet())
		{
			ASPetPlugin.getInstance().getLogger().info("  [WORLD " + worldname + "]:");
			for (ArmorStand stand: map.get(worldname).keySet())
			{
				ASPetPlugin.getInstance().getLogger().info("    Armor Stand: " + stand.getLocation() + " Owner: " 
						+ map.get(worldname).get(stand));
			}
		}
	}

	public static Set<String> getWorlds()
	{
		return map.keySet();
	}
	
	public static void killall()
	{
		for (String worldname: map.keySet())
		{
			for (ArmorStand stand: map.get(worldname).keySet())
			{
				stand.remove();
			}
		}
		map.clear();
	}

	public static void purge()
	{
		int n = 0;
		Map<ArmorStand, String> toRemove = new HashMap<ArmorStand, String>();
		for (String worldname: map.keySet())
		{
			for (ArmorStand stand: map.get(worldname).keySet())
			{
				// Find owner's pet
				String owner = map.get(worldname).get(stand);
				Pet registeredPet = OwnerToPet.get(worldname, owner);
				if (registeredPet == null)
					OwnerToPet.remove(worldname, owner);
				if (registeredPet == null || !stand.equals(registeredPet.getStand()))
					toRemove.put(stand, worldname);
			}
		}

		for (ArmorStand stand: toRemove.keySet())
		{
			map.get(toRemove.get(stand)).remove(stand);
			n++;
		}

		if (Settings.DEBUG)
			ASPetPlugin.getInstance().getLogger().info("Purged " + n + " from StandToOwner.");
	}
}
