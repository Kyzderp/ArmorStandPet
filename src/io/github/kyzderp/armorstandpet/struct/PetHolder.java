/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.struct;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.World;

public class PetHolder 
{
	
	public PetHolder()
	{
		new OwnerToPet();
		new StandToOwner();
		new UnloadedPets();
		
		for (World world: ASPetPlugin.getInstance().getServer().getWorlds())
		{
			String worldname = world.getName();
			OwnerToPet.putWorld(worldname);
			StandToOwner.putWorld(worldname);
			UnloadedPets.putWorld(worldname);
		}
	}
	
	public static void transferWorld(String sourceWorld, String targetWorld, String owner)
	{
		Pet pet = OwnerToPet.remove(sourceWorld, owner);
		if (pet == null)
			return;
		OwnerToPet.put(targetWorld, owner, pet);
		
		StandToOwner.remove(sourceWorld, pet.getStand());
		StandToOwner.put(targetWorld, pet.getStand(), owner);
	}
}

