/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.struct;

import io.github.kyzderp.armorstandpet.ASPetPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.ArmorStand;

public class UnloadedPets 
{
	private static Map<String, Map<String, List<ArmorStand>>> map;
	private static Map<String, Set<ArmorStand>> hidden;

	public UnloadedPets()
	{
		map = new HashMap<String, Map<String, List<ArmorStand>>>();
		hidden = new HashMap<String, Set<ArmorStand>>();
	}

	public static void putWorld(String worldname)
	{
		addWorldIfEmpty(worldname);
	}

	public static Map<String, List<ArmorStand>> getWorld(String worldname)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase());
	}

	public static void put(String worldname, String chunk, List<ArmorStand> stands)
	{
		addWorldIfEmpty(worldname);

		map.get(worldname.toLowerCase()).put(chunk, stands);
	}

	public static List<ArmorStand> get(String worldname, String chunk)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase()).get(chunk);
	}

	public static boolean containsChunk(String worldname, String chunk)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase()).containsKey(chunk);
	}

	public static boolean containsWorld(String worldname)
	{
		return map.containsKey(worldname.toLowerCase());
	}

	public static List<ArmorStand> remove(String worldname, String chunk)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase()).remove(chunk);
	}

	public static void putStand(String worldname, String chunk, ArmorStand stand)
	{
		addWorldIfEmpty(worldname);
		if (!map.get(worldname.toLowerCase()).containsKey(chunk))
			map.get(worldname.toLowerCase()).put(chunk, new ArrayList<ArmorStand>());
		map.get(worldname.toLowerCase()).get(chunk).add(stand);
	}

	public static void hideStand(String worldname, ArmorStand stand)
	{
		addWorldIfEmpty(worldname);
		hidden.get(worldname.toLowerCase()).add(stand);
	}

	public static void showStand(String worldname, ArmorStand stand)
	{
		addWorldIfEmpty(worldname);
		hidden.get(worldname.toLowerCase()).remove(stand);
	}

	private static void addWorldIfEmpty(String worldname) 
	{
		if (!hidden.containsKey(worldname.toLowerCase()))
			hidden.put(worldname.toLowerCase(), new HashSet<ArmorStand>());
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<String, List<ArmorStand>>());
	}

	public static void dump()
	{
		ASPetPlugin.getInstance().getLogger().info("UnloadedPets Dump:");
		for (String worldname: map.keySet())
		{
			ASPetPlugin.getInstance().getLogger().info("  [WORLD " + worldname + "]:");
			for (String chunk: map.get(worldname).keySet())
			{
				ASPetPlugin.getInstance().getLogger().info("    CHUNK " + chunk);
				for (ArmorStand stand: map.get(worldname).get(chunk))
					ASPetPlugin.getInstance().getLogger().info("      " + stand.getLocation());
			}
		}
	}
}
