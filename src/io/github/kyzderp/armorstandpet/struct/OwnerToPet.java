/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.struct;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.HashMap;
import java.util.Map;

public class OwnerToPet 
{
	private static Map<String, Map<String, Pet>> map;
	
	public OwnerToPet()
	{
		map = new HashMap<String, Map<String, Pet>>();
	}
	
	public static void putWorld(String worldname)
	{
		addWorldIfEmpty(worldname);
	}

	public static Map<String, Pet> getWorld(String worldname)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase());
	}
	
	public static void put(String worldname, String owner, Pet pet)
	{
		addWorldIfEmpty(worldname);
		map.get(worldname.toLowerCase()).put(owner, pet);
	}
	
	public static Pet get(String worldname, String owner)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase()).get(owner);
	}
	
	public static boolean containsOwner(String worldname, String owner)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase()).containsKey(owner);
	}
	
	public static boolean containsOwner(String owner)
	{
		for (String world: map.keySet())
		{
			if (map.get(world).containsKey(owner))
				return true;
		}
		return false;
	}
	
	public static boolean containsWorld(String worldname)
	{
		return map.containsKey(worldname.toLowerCase());
	}
	
	public static Pet remove(String worldname, String owner)
	{
		addWorldIfEmpty(worldname);
		return map.get(worldname.toLowerCase()).remove(owner);
	}
	
	private static void addWorldIfEmpty(String worldname) 
	{
		if (!map.containsKey(worldname.toLowerCase()))
			map.put(worldname.toLowerCase(), new HashMap<String, Pet>());
	}
	
	public static void dump()
	{
		ASPetPlugin.getInstance().getLogger().info("OwnerToPet Dump:");
		for (String worldname: map.keySet())
		{
			ASPetPlugin.getInstance().getLogger().info("  [WORLD " + worldname + "]:");
			for (String ownername: map.get(worldname).keySet())
			{
				Pet pet = map.get(worldname).get(ownername);
				ASPetPlugin.getInstance().getLogger().info("    Owner: " + ownername 
						+ " Pet: " + pet.type + " " + pet.getLocationString()); 
			}
		}
	}
}
