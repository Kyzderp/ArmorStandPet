/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.actions.WalkAction;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TellCommand extends BaseCommand 
{

	public TellCommand(ASPetPlugin plugin) 
	{
		super(plugin, "tell");
		this.usage = "/aspet tell <playername> <message>";
		this.description = Lang.get("aspet.tell.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (args.length < 3)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}

		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		// Make sure pet has no task and target isn't too far away
		if (pet.isBusy)
			this.error(sender, Lang.get("aspet.petBusy"));
		else if (!pet.isMobile)
			this.error(sender, Lang.get("aspet.petNotMobile"));
		else
		{
			Player target = Bukkit.getPlayer(args[1]);
			if (target == null || !target.isOnline())
				this.error(sender, Lang.get("aspet.tell.noPlayer"));
			else
			{
				if (pet.distanceSq(target) > Settings.targetRange)
					this.error(sender, Lang.get("aspet.tell.tooFar", target.getName()));
				else
				{
					// Get the message
					String message = "";
					for (int i = 2; i < args.length; i++)
						message += args[i] + " ";
					
					// Set up necessary chain of events
					ASPetAction afterAnnouncing = new NotBusyAction(pet, null);
					ASPetAction afterBack = new SayAction(pet, afterAnnouncing, Lang.get("aspet.tell.petBack"));
					ASPetAction afterTelling = WalkAction.chase(pet, afterBack, (Player)sender, 2, false);
					ASPetAction afterArriving = new SayAction(pet, afterTelling, message.trim());
					ASPetAction startWalking = WalkAction.chase(pet, afterArriving, target, 2, true); 
					
					pet.isBusy = true;
					startWalking.execute();
				}
			}
		}
	}
}
