/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;
import io.github.kyzderp.armorstandpet.types.PetType;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ResetCommand extends BaseCommand 
{

	public ResetCommand(ASPetPlugin plugin) 
	{
		super(plugin, "reset");
		this.usage = "/aspet reset";
		this.description = Lang.get("aspet.reset.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		Player p = (Player)sender;
		Pet prevPet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		Pet pet = Pet.createPet(prevPet.type, prevPet.getOwner(), prevPet.getStand());
		
		pet.getStand().setGravity(false);
		pet.getStand().setVisible(true);
		pet.getStand().setArms(true);
		pet.getStand().setBasePlate(false);
		
		if (pet.type == PetType.NEEDYCHILD && Settings.child_forceSmall)
			pet.getStand().setSmall(true);
		
		ASPetPlugin.inform(sender, Lang.get("aspet.reset.success"));
	}

}
