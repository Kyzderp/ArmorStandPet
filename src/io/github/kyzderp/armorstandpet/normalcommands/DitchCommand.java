/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.hooks.HookManager;
import io.github.kyzderp.armorstandpet.storage.PetSettings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DitchCommand extends BaseCommand 
{
	public DitchCommand(ASPetPlugin plugin) 
	{
		super(plugin, "ditch");
		this.usage = "/aspet ditch";
		this.description = Lang.get("aspet.ditch.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		Player p = (Player)sender;
		String worldname = p.getWorld().getName();
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
		
		boolean canPlace = HookManager.buildingCheck.canPlaceArmorStand(p, pet.getStand().getLocation());
		if (!canPlace)
		{
			ASPetPlugin.error(sender, Lang.get("aspet.ditch.noAccess"));
			return;
		}
		
		this.plugin.savePets(new PetSettings(p.getName(), pet, worldname));
		
		pet.ditch();
		this.plugin.getLogger().info("Player " + sender.getName() + " ditched their pet.");
		
		// Remove from the maps
		StandToOwner.remove(worldname, pet.getStand());
		OwnerToPet.remove(worldname, sender.getName());
		ASPetPlugin.inform(sender, Lang.get("aspet.ditch.success"));
	}

}
