/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.TeleportAction;
import io.github.kyzderp.armorstandpet.actions.WalkAction;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GotoCommand extends BaseCommand 
{

	public GotoCommand(ASPetPlugin plugin) 
	{
		super(plugin, "goto");
		this.usage = "/aspet goto [x y z]";
		this.description = Lang.get("aspet.goto.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		if (args.length == 1)
		{
			if (pet.isBusy)
				this.error(sender, Lang.get("aspet.petBusy"));
			else
			{
				double x = p.getLocation().getBlockX();
				double y = p.getLocation().getBlockY();
				double z = p.getLocation().getBlockZ();

				pet.isBusy = true;
				Location destination = new Location(p.getWorld(), x + 0.5, y, z + 0.5);
				ASPetAction precision = new TeleportAction(pet, new NotBusyAction(pet, null), destination);
				ASPetAction walk = WalkAction.walk(pet, precision, destination, 0.4);
				walk.execute();

				ASPetPlugin.inform(sender, Lang.get("aspet.goto.success",
						new Object[] {x + " " + y + " " + z}));
			}
			return;
		}

		if (args.length != 4)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}

		// Make sure pet has no task
		if (pet.isBusy)
			this.error(sender, Lang.get("aspet.petBusy"));
		else
		{
			try {
				double x = Double.parseDouble(args[1]);
				double y = Double.parseDouble(args[2]);
				double z = Double.parseDouble(args[3]);

				pet.isBusy = true;
				Location destination = new Location(p.getWorld(), x + 0.5, y, z + 0.5);
				ASPetAction precision = new TeleportAction(pet, new NotBusyAction(pet, null), destination);
				ASPetAction walk = WalkAction.walk(pet, precision, destination, 0.4);
				walk.execute();

				ASPetPlugin.inform(sender, Lang.get("aspet.goto.success", x + " " + y + " " + z));
			} catch (NumberFormatException e) {
				this.error(sender, "Usage: " + this.usage);
			}
		}
	}
}
