/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AttrCommand extends BaseCommand 
{

	public AttrCommand(ASPetPlugin plugin) 
	{
		super(plugin, "attr");
		this.description = Lang.get("aspet.attr.desc");
		this.usage = "/aspet attr <arms|baseplate|visible|small> [on|off]";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (!(sender instanceof Player))
			return;
		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
		
		if (args.length == 1)
		{
			// No args, just print out help
			ASPetPlugin.inform(sender, Lang.get("aspet.attr.help") + this.usage);
			return;
		}
		
		if (args.length > 3)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}
		
		boolean toggle = true;
		boolean turnOn = false;
		// Then there is specified on/off
		if (args.length == 3)
		{
			toggle = false;
			if (args[2].matches("(?i)(true|on|yes)"))
				turnOn = true;
			else if (args[2].matches("(?i)(false|off|no)"))
				turnOn = false;
			else
			{
				this.error(sender, "Usage: " + this.usage);
				return;
			}
		}
		
		if (args[1].equalsIgnoreCase("arms"))
		{
			if (toggle)
				pet.getStand().setArms(!pet.getStand().hasArms());
			else if (turnOn)
				pet.getStand().setArms(true);
			else if (!turnOn)
				pet.getStand().setArms(false);
		}
		else if (args[1].equalsIgnoreCase("baseplate"))
		{
			if (toggle)
				pet.getStand().setBasePlate(!pet.getStand().hasBasePlate());
			else if (turnOn)
				pet.getStand().setBasePlate(true);
			else if (!turnOn)
				pet.getStand().setBasePlate(false);
		}
		else if (args[1].equalsIgnoreCase("small"))
		{
			if (toggle)
				pet.getStand().setSmall(!pet.getStand().isSmall());
			else if (turnOn)
				pet.getStand().setSmall(true);
			else if (!turnOn)
				pet.getStand().setSmall(false);
		}
		else if (args[1].equalsIgnoreCase("visible"))
		{
			if (toggle)
				pet.getStand().setVisible(!pet.getStand().isVisible());
			else if (turnOn)
				pet.getStand().setVisible(true);
			else if (!turnOn)
				pet.getStand().setVisible(false);
		}
		else
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}
		
		ASPetPlugin.inform(sender, Lang.get("aspet.attr.success", args[1]));
	}
}
