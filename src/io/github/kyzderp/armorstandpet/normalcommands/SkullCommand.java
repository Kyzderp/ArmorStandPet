/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullCommand extends BaseCommand 
{

	public SkullCommand(ASPetPlugin plugin) 
	{
		super(plugin, "skull");
		this.description = Lang.get("aspet.skull.desc");
		this.usage = "/aspet skull";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (sender instanceof Player)
		{
			Player p = (Player)sender;
			Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
			if (pet != null && pet.getStand() != null)
			{
				if (pet.getStand().getHelmet().getType() == Material.SKULL_ITEM
						&& pet.getStand().getHelmet().getDurability() == 3)
				{
					// Already is a player head, so remove it
					pet.getStand().setHelmet(null);
					ASPetPlugin.inform(sender, Lang.get("aspet.skull.removed"));
					return;
				}
				
				// Else it's not
				ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
				if (skull.getItemMeta() instanceof SkullMeta)
				{
					SkullMeta meta = (SkullMeta)skull.getItemMeta();
					meta.setOwner(p.getName());
					skull.setItemMeta(meta);
				}
				
				// Drop the current
				if (pet.getStand().getHelmet() != null
						&& pet.getStand().getHelmet().getType() != Material.AIR)
					pet.getStand().getWorld().dropItemNaturally(pet.getStand().getLocation(), pet.getStand().getHelmet());
				pet.getStand().setHelmet(skull);
				ASPetPlugin.inform(sender, Lang.get("aspet.skull.set"));
			}
		}
	}
}
