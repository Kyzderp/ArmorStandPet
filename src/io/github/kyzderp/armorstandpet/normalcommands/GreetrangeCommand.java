/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.DoormanPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GreetrangeCommand extends BaseCommand 
{

	public GreetrangeCommand(ASPetPlugin plugin) 
	{
		super(plugin, "greetrange");
		this.description = Lang.get("aspet.greetrange.desc");
		this.usage = "/aspet greetrange <distance>";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (!(sender instanceof Player))
			return;
		Player p = (Player)sender;
		Pet firstpet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
		if (!(firstpet instanceof DoormanPet))
		{
			this.error(sender, Lang.get("aspet.greetrange.notDoorman"));
			return;
		}

		DoormanPet pet = (DoormanPet)firstpet;

		if (args.length == 1)
		{
			ASPetPlugin.inform(sender, Lang.get("aspet.greetrange.currentRange", pet.getRange()));
			return;
		}

		try
		{
			double newrange = Double.parseDouble(args[1]);

			if (newrange > Settings.doorman_maxRange)
			{
				this.error(sender, Lang.get("aspet.greetrange.rangeCap", Settings.doorman_maxRange)); 
				return;
			}

			pet.setRange(newrange);
			ASPetPlugin.inform(sender, Lang.get("aspet.greetrange.success", pet.getRange())); 
		}
		catch (NumberFormatException e) {
			this.error(sender, "Usage: " + this.usage);
		}
	}
}
