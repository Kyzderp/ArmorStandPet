/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.HugAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.actions.WalkAction;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HugCommand extends BaseCommand 
{

	public HugCommand(ASPetPlugin plugin) 
	{
		super(plugin, "hug");
		this.usage = "/aspet hug <playername>";
		this.description = Lang.get("aspet.hug.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (args.length != 2)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}

		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		// Make sure pet has no task and target isn't too far away
		if (pet.isBusy)
			this.error(sender, Lang.get("aspet.petBusy"));
		else if (!pet.isMobile)
			this.error(sender, Lang.get("aspet.petNotMobile"));
		else
		{
			Player target = Bukkit.getPlayer(args[1]);
			if (target == null || !target.isOnline())
				this.error(sender, Lang.get("aspet.noPlayer"));
			else
			{
				if (pet.distanceSq(target) > Settings.targetRange)
					this.error(sender, Lang.get("aspet.hug.tooFar", target.getName())); 
				else
				{
					// Set up chain of events
					ASPetAction afterAnnouncing = new NotBusyAction(pet, null);
					ASPetAction afterBack = new SayAction(pet, afterAnnouncing, Lang.get("aspet.hug.petBack"));
					ASPetAction afterTelling = WalkAction.chase(pet, afterBack, (Player)sender, 2, false); 
					ASPetAction afterHugging = new SayAction(pet, afterTelling, Lang.get("aspet.hug.hug"));
					ASPetAction afterArriving = new HugAction(pet, afterHugging);
					ASPetAction startWalking = WalkAction.chase(pet,afterArriving, target, 1, true); 
					
					pet.isBusy = true;
					startWalking.execute();
				}
			}
		}
	}
}
