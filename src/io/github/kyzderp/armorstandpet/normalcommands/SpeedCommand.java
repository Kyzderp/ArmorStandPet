/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpeedCommand extends BaseCommand 
{

	public SpeedCommand(ASPetPlugin plugin) 
	{
		super(plugin, "speed");
		this.usage = "/aspet speed <number>";
		this.description = Lang.get("aspet.speed.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (args.length < 2)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}

		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		// Make sure pet has no task and target isn't too far away
		if (pet.isBusy)
			this.error(sender, Lang.get("aspet.petBusy"));
		else
		{
			try {
				double speed = Double.parseDouble(args[1]);
				if (speed < Settings.minSpeed || speed > Settings.maxSpeed)
				{
					this.error(sender, Lang.get("aspet.speed.range",
							new Object[] {Settings.minSpeed, Settings.maxSpeed}));
					return;
				}
				pet.setSpeed(speed);
				ASPetPlugin.inform(sender, Lang.get("aspet.speed.success", speed));
			} catch (NumberFormatException e) {
				this.error(sender, "Usage: " + this.usage);
			}
		}
	}
}
