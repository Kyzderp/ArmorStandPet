/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SayCommand extends BaseCommand 
{

	public SayCommand(ASPetPlugin plugin) 
	{
		super(plugin, "say");
		this.usage = "/aspet say <message>";
		this.description = Lang.get("aspet.say.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (args.length == 1)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}

		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		// Make sure pet is not already doing something
		if (pet.isBusy)
			this.error(sender, Lang.get("aspet.petBusy"));
		else
		{
			// Get the message
			String message = "";
			for (int i = 1; i < args.length; i++)
				message += args[i] + " ";
			pet.say(message.trim());
		}
	}
}
