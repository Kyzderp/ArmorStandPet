/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DeleteCommand extends BaseCommand 
{

	public DeleteCommand(ASPetPlugin plugin) 
	{
		super(plugin, "delete");
		this.usage = "/aspet delete";
		this.description = Lang.get("aspet.delete.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		Player p = (Player)sender;
		String worldname = p.getWorld().getName();
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
		
		pet.ditch();
		this.plugin.getLogger().info("Player " + sender.getName() + " deleted their pet in "
				+ worldname + ".");
		
		pet.delete(true, true);
		ASPetPlugin.inform(sender, Lang.get("aspet.delete.success"));
	}

}
