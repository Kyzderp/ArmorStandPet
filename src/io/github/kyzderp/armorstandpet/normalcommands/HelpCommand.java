/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;

import java.util.Map;

import org.bukkit.command.CommandSender;

public class HelpCommand extends BaseCommand 
{
	private Map<String, BaseCommand> commands;

	public HelpCommand(ASPetPlugin plugin, Map<String, BaseCommand> commands) 
	{
		super(plugin, "help");
		this.commands = commands;
		this.usage = "/aspet help";
		this.description = Lang.get("aspet.help.desc");
		this.requiresPerm = false;
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		String[] toDisplay = new String[this.commands.size() + 1];
		String[] keys = this.commands.keySet().toArray(new String[0]);
		for (int i = 0; i < keys.length; i++)
			toDisplay[i + 1] = this.formatHelp(sender, keys[i]);

		toDisplay[0] = "\u00A78[\u00A7a!\u00A78] \u00A7aInfo \u00A78[\u00A7a!\u00A78]"
				+ " \u00A77ArmorStandPet commands:";

		sender.sendMessage(toDisplay);
	}

	private String formatHelp(CommandSender sender, String cmd)
	{
		BaseCommand command = this.commands.get(cmd);
		if (!command.requiresPerm || sender.hasPermission("armorstandpet.command." + cmd))
		{
			String result = "\u00A7a" + command.usage + " \u00A7f- \u00A77";
			result += command.description;
			return result;
		}
		return "";
	}
}
