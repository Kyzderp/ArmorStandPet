/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.actions.WalkAction;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InsultCommand extends BaseCommand 
{

	public InsultCommand(ASPetPlugin plugin) 
	{
		super(plugin, "insult");
		this.usage = "/aspet insult <playername>";
		this.description = Lang.get("aspet.insult.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (args.length != 2)
		{
			this.error(sender, "Usage: " + this.usage);
			return;
		}

		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		// Make sure pet has no task and target isn't too far away
		if (pet.isBusy)
			this.error(sender, Lang.get("aspet.petBusy"));
		else if (!pet.isMobile)
			this.error(sender, Lang.get("aspet.petNotMobile"));
		else
		{
			Player target = Bukkit.getPlayer(args[1]);
			if (target == null || !target.isOnline())
				this.error(sender, Lang.get("aspet.insult.noPlayer"));
			else
			{
				if (pet.distanceSq(target) > Settings.targetRange)
					this.error(sender, Lang.get("aspet.insult.tooFar", target.getName())); 
				else
				{
					// Set up chain of events
					ASPetAction afterAnnouncing = new NotBusyAction(pet, null);
					ASPetAction afterBack = new SayAction(pet, afterAnnouncing, Lang.get("aspet.insult.petBack"));
					ASPetAction afterTelling = WalkAction.chase(pet, afterBack, (Player)sender, 2, false); 
					ASPetAction afterArriving = new SayAction(pet, afterTelling, pet.getInsult(target.getName()));
					ASPetAction startWalking = WalkAction.chase(pet, afterArriving, target, 2, true); 
					
					pet.isBusy = true;
					startWalking.execute();
				}
			}
		}
	}
}
