/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.actions.WalkAction;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CallCommand extends BaseCommand 
{

	public CallCommand(ASPetPlugin plugin) 
	{
		super(plugin, "call");
		this.description = Lang.get("aspet.call.desc");
		this.usage = "/aspet call";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
			if (pet.isBusy)
				this.error(sender, Lang.get("aspet.petBusy"));
			else if (!pet.isMobile)
				this.error(sender, Lang.get("aspet.petNotMobile"));
			else
			{
				// Set up chain of events
				ASPetAction afterAnnouncing = new NotBusyAction(pet, null); 
				ASPetAction afterArriving = new SayAction(pet, afterAnnouncing, 
						pet.getAnnounce(pet.getOwner()));
				ASPetAction startWalking = WalkAction.chase(pet, afterArriving, p, 2, false);
//				ASPetAction startWalking = WalkAction.walk(pet, afterArriving, 
//						((Player)sender).getLocation(), 0.2); 
				ASPetAction sayComing = new SayAction(pet, null, Lang.get("aspet.call.petSay"));

				// Execute
				pet.isBusy = true;
				startWalking.execute();
				sayComing.execute();
				ASPetPlugin.inform(sender, Lang.get("aspet.call.success"));
			}
		}
	}
}
