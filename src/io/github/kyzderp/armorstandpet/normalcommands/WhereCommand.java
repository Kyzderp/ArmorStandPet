/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WhereCommand extends BaseCommand 
{

	public WhereCommand(ASPetPlugin plugin) 
	{
		super(plugin, "where");
		this.description = Lang.get("aspet.where.desc");
		this.usage = "/aspet where";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (sender instanceof Player)
		{
			Player p = (Player)sender;
			Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
			ASPetPlugin.inform(sender, Lang.get("aspet.where.result",
					new Object[] { 
					pet.getStand().getLocation().getBlockX(),
					pet.getStand().getLocation().getBlockY(),
					pet.getStand().getLocation().getBlockZ()}));
		}
	}
}
