/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AnnouncesCommand extends BaseCommand 
{

	public AnnouncesCommand(ASPetPlugin plugin) 
	{
		super(plugin, "announces");
		this.description = Lang.get("aspet.announces.desc");
		this.usage = "/aspet announces <add|remove|list>";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (!(sender instanceof Player))
			return;
		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
		
		if (args.length == 1)
		{
			// No args, just print out help
			sender.sendMessage(new String[] {Lang.get("aspet.announces.helpHeader"),
					"\u00A77/aspet announces list \u00A7f- \u00A77see the list of current announcements",
					"\u00A77/aspet announces add <message> \u00A7f- \u00A77add an announcement to the list",
					"\u00A77/aspet announces remove <number> \u00A7f- \u00A77remove the specified announcement",
					});
			return;
		}
		
		if (args[1].equalsIgnoreCase("list"))
		{
			String[] toDisplay = new String[pet.announces.size() + 1];
			toDisplay[0] = Lang.get("aspet.announces.currentHeader");
			for (int i = 0; i < toDisplay.length - 1; i++)
				toDisplay[i + 1] = "\u00A77<" + (i + 1) + "> " + pet.announces.get(i);
			sender.sendMessage(toDisplay);
			return;
		}
		
		if (args[1].equalsIgnoreCase("add"))
		{
			if (args.length < 3)
			{
				this.error(sender, "Usage: /aspet announces add <message>");
				return;
			}
			String announcement = "";
			for (int i = 2; i < args.length; i++)
				announcement += args[i] + " ";
			announcement = announcement.trim();
			
			pet.announces.add(announcement);
			ASPetPlugin.inform(sender, Lang.get("aspet.announces.added", announcement));
			return;
		}
		
		if (args[1].equalsIgnoreCase("remove"))
		{
			if (args.length != 3)
			{
				this.error(sender, "Usage: /aspet announces remove <number>");
				return;
			}
			try
			{
				int index = Integer.parseInt(args[2]);
				if (index < 1 || index > pet.announces.size())
				{
					this.error(sender, Lang.get("aspet.announces.invalidEntry", index)); 
					return;
				}
				
				index--;
				ASPetPlugin.inform(sender, Lang.get("aspet.announces.removed",
						pet.announces.remove(index))); 
			} 
			catch (NumberFormatException e) {
				this.error(sender, "Usage: /aspet announces remove <number>");
				return;
			}
			return;
		}
	}
}
