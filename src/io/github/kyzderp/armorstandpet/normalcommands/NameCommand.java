/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.tasks.RevertNameTask;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NameCommand extends BaseCommand 
{

	public NameCommand(ASPetPlugin plugin) 
	{
		super(plugin, "name");
		this.usage = "/aspet name <name>";
		this.description = Lang.get("aspet.name.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		// Clear the name
		if (args.length == 1)
		{
			pet.setName("");
			(new RevertNameTask(pet, new NotBusyAction(pet, null))).runTask(this.plugin);
			ASPetPlugin.inform(sender, Lang.get("aspet.name.cleared"));
		}
		else
		{
			// Get the message
			String name = "";
			for (int i = 1; i < args.length; i++)
				name += args[i] + " ";

			pet.setName(name.trim());
			(new RevertNameTask(pet, new NotBusyAction(pet, null))).runTask(this.plugin);
			ASPetPlugin.inform(sender, Lang.get("aspet.name.success", pet.getName().trim()));
		}
	}
}
