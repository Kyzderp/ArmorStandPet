/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ASPetCommand implements CommandExecutor
{
	private ASPetPlugin plugin;
	private Map<String, BaseCommand> commands;

	public ASPetCommand(ASPetPlugin plugin)
	{
		this.plugin = plugin;
		this.commands = new LinkedHashMap<String, BaseCommand>();
		// call|insult|say|tell|ditch|sit|hug|name|speed|where
		this.commands.put("call", new CallCommand(this.plugin));
		this.commands.put("delete", new DeleteCommand(this.plugin));
		this.commands.put("ditch", new DitchCommand(this.plugin));
		this.commands.put("goto", new GotoCommand(this.plugin));
		this.commands.put("hug", new HugCommand(this.plugin));
		this.commands.put("insult", new InsultCommand(this.plugin));
		this.commands.put("name", new NameCommand(this.plugin));
		this.commands.put("reset", new ResetCommand(this.plugin));
		this.commands.put("say", new SayCommand(this.plugin));
		this.commands.put("sit", new SitCommand(this.plugin));
		this.commands.put("speed", new SpeedCommand(this.plugin));
		this.commands.put("tell", new TellCommand(this.plugin));
		this.commands.put("where", new WhereCommand(this.plugin));
		
		// insults|announces|greetings|greetrange
		this.commands.put("skull", new SkullCommand(this.plugin));
		this.commands.put("attr", new AttrCommand(this.plugin));
		this.commands.put("insults", new InsultsCommand(this.plugin));
		this.commands.put("announces", new AnnouncesCommand(this.plugin));
		this.commands.put("greetings", new GreetingsCommand(this.plugin));
		this.commands.put("greetrange", new GreetrangeCommand(this.plugin));

		// HALP.
		this.commands.put("help", new HelpCommand(this.plugin, this.commands));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
//		if (!cmd.getName().equals("aspet"))
//			return false;
		
		if (!(sender instanceof Player))
		{
			ASPetPlugin.error(sender, "Only players can own pets!");
			return true;
		}

		// No arguments
		if (args.length == 0)
		{
			ASPetPlugin.inform(sender, "\u00A7fArmorStandPet \u00A77by \u00A7fKyzeragon. "
					+ Lang.get("aspet.seeHelp"));
			return true;
		}

		// The player should have a pet for the rest of the commands
		if (!OwnerToPet.containsOwner(((Player)sender).getWorld().getName(), sender.getName()))
		{
			ASPetPlugin.error(sender, Lang.get("aspet.needPet"));
			return true;
		}

		// See if it's a valid command
		BaseCommand command = this.commands.get(args[0].toLowerCase());
		if (command == null)
		{
			ASPetPlugin.error(sender, Lang.get("aspet.invalidSyntax"));
			return true;
		}

		// Run the actual command
		command.execute(sender, args);

		return true;
	}

}
