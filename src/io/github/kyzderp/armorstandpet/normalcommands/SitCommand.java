/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.actions.SitAction;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SitCommand extends BaseCommand 
{

	public SitCommand(ASPetPlugin plugin) 
	{
		super(plugin, "sit");
		this.usage = "/aspet sit";
		this.description = Lang.get("aspet.sit.desc");
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		Player p = (Player)sender;
		Pet pet = OwnerToPet.get(p.getWorld().getName(), sender.getName());

		// Sit down
		ASPetAction sit = new SitAction(pet, null);
		ASPetAction say = new SayAction(pet, null, Lang.get("aspet.sit.petSay"));
		sit.execute();
		say.execute();
	}

}
