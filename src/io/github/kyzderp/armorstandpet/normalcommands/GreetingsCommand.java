/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.normalcommands;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Lang;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.types.DoormanPet;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GreetingsCommand extends BaseCommand 
{

	public GreetingsCommand(ASPetPlugin plugin) 
	{
		super(plugin, "greetings");
		this.description = Lang.get("aspet.greetings.desc");
		this.usage = "/aspet greetings <add|remove|list>";
	}

	@Override
	public void run(CommandSender sender, String[] args) 
	{
		if (!(sender instanceof Player))
			return;
		Player p = (Player)sender;
		Pet firstpet = OwnerToPet.get(p.getWorld().getName(), sender.getName());
		if (!(firstpet instanceof DoormanPet))
		{
			this.error(sender, Lang.get("aspet.greetings.notDoorman"));
			return;
		}
		
		DoormanPet pet = (DoormanPet)firstpet;
		
		if (args.length == 1)
		{
			// No args, just print out help
			sender.sendMessage(new String[] {Lang.get("aspet.greetings.helpHeader"),
					"\u00A77/aspet greetings list \u00A7f- \u00A77see the list of current greetings",
					"\u00A77/aspet greetings add <message> \u00A7f- \u00A77add an greeting to the list",
					"\u00A77/aspet greetings remove <number> \u00A7f- \u00A77remove the specified greeting",
					});
			return;
		}
		
		if (args[1].equalsIgnoreCase("list"))
		{
			String[] toDisplay = new String[pet.greetings.size() + 1];
			toDisplay[0] = Lang.get("aspet.greetings.currentHeader");
			for (int i = 0; i < toDisplay.length - 1; i++)
				toDisplay[i + 1] = "\u00A77<" + (i + 1) + "> " + pet.greetings.get(i);
			sender.sendMessage(toDisplay);
			return;
		}
		
		if (args[1].equalsIgnoreCase("add"))
		{
			if (args.length < 3)
			{
				this.error(sender, "Usage: /aspet greetings add <message>");
				return;
			}
			String greeting = "";
			for (int i = 2; i < args.length; i++)
				greeting += args[i] + " ";
			greeting = greeting.trim();
			
			pet.greetings.add(greeting);
			ASPetPlugin.inform(sender, Lang.get("aspet.greetings.added", greeting));
			return;
		}
		
		if (args[1].equalsIgnoreCase("remove"))
		{
			if (args.length != 3)
			{
				this.error(sender, "Usage: /aspet greetings remove <number>");
				return;
			}
			try
			{
				int index = Integer.parseInt(args[2]);
				if (index < 1 || index > pet.greetings.size())
				{
					this.error(sender, Lang.get("aspet.greetings.invalidEntry", index));
					return;
				}
				
				index--;
				ASPetPlugin.inform(sender, Lang.get("aspet.greetings.removed",
						pet.greetings.remove(index)));
			} 
			catch (NumberFormatException e) {
				this.error(sender, "Usage: /aspet greetings remove <number>");
				return;
			}
			return;
		}
	}
}
