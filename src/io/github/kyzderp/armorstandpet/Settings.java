/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Settings 
{
	private JavaPlugin plugin;
	
	public static boolean DEBUG = false;
	
	public static boolean removeDupes;
	public static int autosave;
	
	public static String AI;
	public static int maxPathRadius;
	public static Set<Material> nonSolid;
	public static boolean continuous; // TODO
	
	// Whether pets will follow across worlds
	public static boolean crossWorld;
	
	// Whether pets will be hidden when owner is logged out (does not apply to doormen)
	public static boolean hideLogout;
	
	// Squared values, for faster calculations
	public static double followRange;
	public static double teleportRange;
	public static double targetRange;
	
	// Some more default values
	public static double minSpeed;
	public static double maxSpeed;
	public static List<String> defaultAnnounces;
	public static List<String> defaultInsults;
	public static boolean forceNoPlate;
	public static boolean forceArms;
	
	// Which types are enabled
	public static Map<String, Boolean> typeEnables;
	
	// NeedyChild-specific settings
	public static boolean child_forceSmall;
	
	// Doorman-specific settings
	public static double doorman_maxRange;
	public static int doorman_cooldown;
	
	// SillyWalker-specific settings
	

	public Settings(JavaPlugin plugin)
	{
		this.plugin = plugin;
	}

	//////////////////////// LOAD ///////////////////////
	public void loadSettings()
	{
		FileConfiguration config = this.plugin.getConfig();
		config.options().copyDefaults(true);
		
		DEBUG = config.getBoolean("debug");
		removeDupes = config.getBoolean("removeDupesOnLoad");
		autosave = config.getInt("autosave");
		
		AI = config.getString("ai.type");
		maxPathRadius = config.getInt("ai.maxPathRadius");
		continuous = false;
//		continuous = config.getBoolean("ai.continuous");
		nonSolid = new HashSet<Material>();
		List<String> mats = config.getStringList("ai.nonSolid");
		for (String mat: mats)
		{
			Material material = Material.getMaterial(mat.toUpperCase());
			if (material != null)
				nonSolid.add(material);
		}
		
		crossWorld = config.getBoolean("crossWorldPets");
		hideLogout = config.getBoolean("hideWhenLogout");
		
		followRange = config.getDouble("followRange");
		followRange = followRange * followRange;
		teleportRange = config.getDouble("teleportRange");
		teleportRange = teleportRange * teleportRange;
		targetRange = config.getDouble("targetRange");
		targetRange = targetRange * targetRange;
		
		minSpeed = config.getDouble("minSpeed");
		maxSpeed = config.getDouble("maxSpeed");
		defaultAnnounces = config.getStringList("defaultAnnounces");
		defaultInsults = config.getStringList("defaultInsults");
		forceNoPlate = config.getBoolean("forceNoBaseplate");
		forceArms = config.getBoolean("forceArms");
		
		Map<String, Object> petTypes = config.getConfigurationSection("petTypes").getValues(false);
		typeEnables = new HashMap<String, Boolean>();
		for (String key: petTypes.keySet())
		{
			ConfigurationSection section = (ConfigurationSection) petTypes.get(key);
			typeEnables.put(key, section.getBoolean("enabled"));
		}
		
		child_forceSmall = config.getBoolean("petTypes.needychild.forceSmall");
		
		doorman_maxRange = config.getDouble("petTypes.doorman.maxRange");
		doorman_cooldown = config.getInt("petTypes.doorman.cooldown");
	}
}
