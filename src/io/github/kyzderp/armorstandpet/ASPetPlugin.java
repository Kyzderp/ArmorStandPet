/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet;

import io.github.kyzderp.armorstandpet.admincommands.ASPetAdminCommand;
import io.github.kyzderp.armorstandpet.gui.ASPetGUIListener;
import io.github.kyzderp.armorstandpet.hooks.HookManager;
import io.github.kyzderp.armorstandpet.listeners.ChunkListener;
import io.github.kyzderp.armorstandpet.listeners.PetRespawnListener;
import io.github.kyzderp.armorstandpet.listeners.PlayerActionListener;
import io.github.kyzderp.armorstandpet.normalcommands.ASPetCommand;
import io.github.kyzderp.armorstandpet.storage.PetConfigDoesNotExistException;
import io.github.kyzderp.armorstandpet.storage.PetSettings;
import io.github.kyzderp.armorstandpet.storage.PetStorage;
import io.github.kyzderp.armorstandpet.storage.PlayerFileDoesNotExistException;
import io.github.kyzderp.armorstandpet.struct.PetHolder;
import io.github.kyzderp.armorstandpet.types.Pet;
import io.github.kyzderp.armorstandpet.types.PetType;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyzeragon 4/24/16
 */

public class ASPetPlugin extends JavaPlugin 
{
	public static boolean useDualWield;
	
	private static ASPetPlugin instance;
	
	private PetStorage petStorage;

	private Settings settings;
	private PlayerActionListener playerListener;
	private ChunkListener chunkListener;
	private PetRespawnListener petRespawnListener;
	private ASPetGUIListener guiListener;
	
	private AutosaveTask autosaveTask;

	@Override
	public void onEnable()
	{
		String version = getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
        useDualWield = !version.startsWith("v1_8");
        if (useDualWield)
        	this.getLogger().info("Detected not-1.8, enabling dual wield support.");
		
		this.saveDefaultConfig();
		this.reloadConfig();

		instance = this;
		new PetHolder();

		this.settings = new Settings(this);
		this.settings.loadSettings();
		Lang.loadStrings(this);
		
		this.petStorage = new PetStorage(this);
		this.petStorage.loadAll(false);

		// Listeners
		this.playerListener = new PlayerActionListener(this);
		this.getServer().getPluginManager().registerEvents(this.playerListener, this);
		
		this.chunkListener = new ChunkListener(this);
		this.getServer().getPluginManager().registerEvents(this.chunkListener, this);
		
		this.petRespawnListener = new PetRespawnListener(this);
		this.getServer().getPluginManager().registerEvents(this.petRespawnListener, this);

		this.guiListener = new ASPetGUIListener();
		this.getServer().getPluginManager().registerEvents(this.guiListener, this);

		// Commands
		this.getCommand("aspet").setExecutor(new ASPetCommand(this));
		this.getCommand("aspetadmin").setExecutor(new ASPetAdminCommand(this));
		
		// Autosave task
		this.autosaveTask = new AutosaveTask();
		if (Settings.autosave > 0)
			this.autosaveTask.runTaskTimer(this, Settings.autosave * 20, Settings.autosave * 20);
		
		// Hooks
		new HookManager();
	}

	@Override
	public void onDisable()
	{
		this.autosaveTask.cancel();
		this.petStorage.saveAllPets(true);
	}

	public ASPetGUIListener getGuiListener()
	{
		return this.guiListener;
	}

	public static ASPetPlugin getInstance()
	{
		return instance;
	}

	public static void inform(CommandSender player, String message)
	{
		if (!message.equals(""))
			player.sendMessage(Lang.get("informPrefix") + message.replaceAll("&", "\u00A7"));
	}
	
	public static void informRaw(CommandSender player, String[] messages)
	{
		for (String message: messages)
			message = message.replaceAll("&", "\u00A7");
		player.sendMessage(messages);
	}

	public static void error(CommandSender player, String message)
	{
		if (!message.equals(""))
			player.sendMessage(Lang.get("errorPrefix") + message.replaceAll("&", "\u00A7"));
	}
	
	public Settings getSettings()
	{
		return this.settings;
	}
	
	public Pet loadPetSettings(String owner, String world, PetType type, ArmorStand stand) 
			throws PlayerFileDoesNotExistException, PetConfigDoesNotExistException
	{
		Pet pet = this.petStorage.loadPetSettings(owner, world, type, stand);

		return pet;
	}
	
	public void savePets(PetSettings petSettings)
	{
		this.petStorage.savePets(petSettings);
	}
	
	public void saveAllPets()
	{
		this.petStorage.saveAllPets(false);
	}
	
	public void loadPets(boolean preserve)
	{
		this.petStorage.loadAll(preserve);
	}
}
