/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions;

import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

import org.bukkit.Location;

public class TeleportAction extends ASPetAction 
{
	private Location loc;

	public TeleportAction(Pet pet, ASPetAction callback, Location loc) 
	{
		super(pet, callback);
		this.loc = loc;
	}

	public TeleportAction(Pet pet, List<ASPetAction> callback, Location loc) 
	{
		super(pet, callback);
		this.loc = loc;
	}

	@Override
	public void execute() 
	{
		this.pet.stand();

		this.pet.getStand().teleport(this.loc);
		this.done();
		return;

	}

}
