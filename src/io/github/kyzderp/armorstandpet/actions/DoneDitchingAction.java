/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.types.Pet;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

public class DoneDitchingAction extends ASPetAction 
{
	public DoneDitchingAction(Pet pet, ASPetAction callback)
	{
		super(pet, callback);
	}

	@Override
	public void execute() 
	{
		if (Settings.DEBUG)
			ASPetPlugin.getInstance().getLogger().info("Reverting pet's armor stand...");

		// Reset armor stand settings
		this.pet.getStand().setArms(false);
		this.pet.getStand().setBasePlate(true);
		this.pet.getStand().setGravity(false);
		this.pet.getStand().setVisible(true);
		this.pet.getStand().setSmall(false);

		// Get rid of name
		this.pet.getStand().setCustomName("");
		this.pet.getStand().setCustomNameVisible(false);

		World world = this.pet.getStand().getWorld();
		// Drop the item in hands
		if (this.pet.getStand().getItemInHand() != null
				&& this.pet.getStand().getItemInHand().getType() != Material.AIR)
		{
			world.dropItem(this.pet.getStand().getLocation(), this.pet.getStand().getItemInHand());
			this.pet.getStand().setItemInHand(new ItemStack(Material.AIR));
		}
		if (ASPetPlugin.useDualWield && this.pet.getStand().getEquipment().getItemInOffHand() != null
				&& this.pet.getStand().getEquipment().getItemInOffHand().getType() != Material.AIR)
		{
			world.dropItem(this.pet.getStand().getLocation(), 
					this.pet.getStand().getEquipment().getItemInOffHand());
			this.pet.getStand().getEquipment().setItemInOffHand(new ItemStack(Material.AIR));
		}

		// Back to standing position
		this.pet.stand();

		this.done();
	}
}
