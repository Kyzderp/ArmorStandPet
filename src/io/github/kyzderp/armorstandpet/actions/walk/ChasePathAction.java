/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions.walk;

import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.ai.Path;
import io.github.kyzderp.armorstandpet.tasks.ChasePathTask;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

import org.bukkit.entity.Player;

public class ChasePathAction extends ASPetAction 
{
	private Path path;
	private double distance;
	private Player player;
	private Double playerDistance;
	private boolean imperative;
	
	public ChasePathAction(Pet pet, ASPetAction callback, Path path, double distance,
			Player player, Double playerDistance, boolean imperative) 
	{
		super(pet, callback);
		this.path = path;
		this.distance = distance;
		this.player = player;
		this.playerDistance = playerDistance;
		this.imperative = imperative;
	}

	public ChasePathAction(Pet pet, List<ASPetAction> callback, Path path, double distance,
			Player player, Double playerDistance, boolean imperative) 
	{
		super(pet, callback);
		this.path = path;
		this.distance = distance;
		this.player = player;
		this.playerDistance = playerDistance;
		this.imperative = imperative;
	}

	public ChasePathAction(Pet pet, ASPetAction callback, Path path, double distance,
			Player player, Double playerDistance) 
	{
		super(pet, callback);
		this.path = path;
		this.distance = distance;
		this.player = player;
		this.playerDistance = playerDistance;
	}

	public ChasePathAction(Pet pet, List<ASPetAction> callback, Path path, double distance,
			Player player, Double playerDistance) 
	{
		super(pet, callback);
		this.path = path;
		this.distance = distance;
		this.player = player;
		this.playerDistance = playerDistance;
	}

	@Override
	public void execute() 
	{
		(new ChasePathTask(this.pet, this.path, this.callback, 
				this.distance, 0, this.player, this.playerDistance, this.imperative)).run();
	}
}
