/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions.walk;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.tasks.WalkLocTask;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class WalkLocAction extends ASPetAction 
{
	private Location destination;
	private double distance;
	private Player player;
	private double playerDistance;
	
	public WalkLocAction(Pet pet, ASPetAction callback, 
			Location destination, double distance, Player player, double playerDistance) 
	{
		super(pet, callback);
		this.destination = destination;
		this.distance = distance;
		this.player = player;
		this.playerDistance = playerDistance;
	}

	public WalkLocAction(Pet pet, ASPetAction callback, Location destination, double distance, Player player) 
	{
		super(pet, callback);
		this.destination = destination;
		this.distance = distance;
		this.player = player;
	}

	public WalkLocAction(Pet pet, List<ASPetAction> callback, Location destination, double distance, Player player) 
	{
		super(pet, callback);
		this.destination = destination;
		this.distance = distance;
		this.player = player;
	}

	@Override
	public void execute() 
	{
		if (this.player != null)
		{
			double currDist = this.pet.distanceSq(this.player);

			if (currDist < this.distance)
			{
				this.done();
				return;
			}
			else if (currDist > Settings.teleportRange)
			{
				this.pet.teleportToFrontOf(this.player.getLocation(), this.playerDistance);
				this.pet.facePlayer(this.player);
				this.done();
				return;
			}
		}

		if (Settings.DEBUG)
			ASPetPlugin.getInstance().getLogger().info("Sending pet to " + this.destination);
		this.pet.stand();
		(new WalkLocTask(this.pet, this.destination, this.callback, this.distance))
		.runTask(ASPetPlugin.getInstance());
	}

}
