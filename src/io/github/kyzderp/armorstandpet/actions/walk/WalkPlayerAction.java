/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions.walk;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.tasks.WalkPlayerTask;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.List;

import org.bukkit.entity.Player;

public class WalkPlayerAction extends ASPetAction 
{
	private Player destination;
	private double distance;
	
	public WalkPlayerAction(Pet pet, ASPetAction callback, Player destination, double distance) 
	{
		super(pet, callback);
		this.destination = destination;
		this.distance = distance;
	}
	
	public WalkPlayerAction(Pet pet, List<ASPetAction> callback, Player destination, double distance) 
	{
		super(pet, callback);
		this.destination = destination;
		this.distance = distance;
	}

	@Override
	public void execute() 
	{
		if (Settings.DEBUG)
			ASPetPlugin.getInstance().getLogger().info("Sending pet after " + destination.getDisplayName());
		this.pet.stand();
		(new WalkPlayerTask(this.pet, this.destination, this.callback, this.distance))
		.runTask(ASPetPlugin.getInstance());
	}

}