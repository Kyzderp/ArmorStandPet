/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.walk.WalkLocAction;
import io.github.kyzderp.armorstandpet.actions.walk.ChasePathAction;
import io.github.kyzderp.armorstandpet.actions.walk.WalkPlayerAction;
import io.github.kyzderp.armorstandpet.ai.Path;
import io.github.kyzderp.armorstandpet.ai.algorithms.PathFinder;
import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;


public class WalkAction
{
	public static ASPetAction walk(Pet pet, ASPetAction callback, 
			Location destination, double distance)
	{
		List<ASPetAction> callbackList = new ArrayList<ASPetAction>();
		callbackList.add(callback);
		return walk(pet, callbackList, destination, distance);
	}


	public static ASPetAction walk(Pet pet, List<ASPetAction> callback, 
			Location destination, double distance)
	{
		if (Settings.AI.equalsIgnoreCase("direct"))
			return new WalkLocAction(pet, callback, destination, distance, null);
		else
		{
			// Some form of pathfinding
			if (pet.isSitting)
				pet.stand();

			PathFinder pathFinder = PathFinder.getPathfinder(pet.getStand().getLocation(), 
					destination, Settings.AI);
			Path path = pathFinder.findPath();
			if (Settings.DEBUG)
				ASPetPlugin.getInstance().getLogger().info(path.dump());

//			return new WalkPathAction(pet, callback, path);
			return new ChasePathAction(pet, callback, path, distance, null, null);
		}
	}
	
	public static ASPetAction chase(Pet pet, ASPetAction callback, 
			Player player, double distance, boolean imperative)
	{
		List<ASPetAction> callbackList = new ArrayList<ASPetAction>();
		callbackList.add(callback);
		return chase(pet, callbackList, player, distance, imperative);
	}

	/**
	 * Chase the player
	 * @param pet The pet
	 * @param callback Actions to perform after chasing
	 * @param player The target player
	 * @param distance The distance from the player that is considered ok
	 * @param imperative Whether to pursue closely, i.e. teleport if flying?
	 * @return
	 */
	public static ASPetAction chase(Pet pet, List<ASPetAction> callback, 
			Player player, double distance, boolean imperative)
	{
		if (Settings.AI.equalsIgnoreCase("direct"))
			return new WalkPlayerAction(pet, callback, player, distance);
		else
		{
			// Some form of pathfinding
			if (pet.isSitting)
				pet.stand();

			PathFinder pathFinder = PathFinder.getPathfinder(pet.getStand().getLocation(), 
					player.getLocation(), Settings.AI);
			Path path = pathFinder.findPath();
			if (Settings.DEBUG)
				ASPetPlugin.getInstance().getLogger().info(path.dump());

			return new ChasePathAction(pet, callback, path, 0.4, player, distance, imperative);
		}
	}
}
