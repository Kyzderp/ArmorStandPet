/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.types.Pet;

public class SayAction extends ASPetAction 
{
	private String message;
	
	public SayAction(Pet pet, ASPetAction callback, String message) 
	{
		super(pet, callback);
		this.message = message;
	}

	@Override
	public void execute() 
	{
		if (Settings.DEBUG)
			ASPetPlugin.getInstance().getLogger().info("Making pet say: " + this.message);
		this.pet.displayMessage(this.message, this.callback);
	}
}
