/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.actions;

import io.github.kyzderp.armorstandpet.types.Pet;

import java.util.ArrayList;
import java.util.List;


public abstract class ASPetAction 
{
	protected Pet pet;
	protected List<ASPetAction> callback;

	/**
	 * Constructor
	 * @param callback Upon completion of this action, do this
	 */
	public ASPetAction(Pet pet, ASPetAction callback)
	{
		this.pet = pet;
		if (callback == null)
			this.callback = null;
		else
		{
			this.callback = new ArrayList<ASPetAction>();
			this.callback.add(callback);
		}
	}

	/**
	 * Constructor
	 * @param callback Upon completion of this action, do this list of callbacks
	 */
	public ASPetAction(Pet pet, List<ASPetAction> callbacks)
	{
		this.pet = pet;
		this.callback = callbacks;
	}

	public abstract void execute();

	/**
	 * Call this function when done with execution to do callbacks
	 */
	protected void done()
	{
		if (this.callback != null)
		{
			for (ASPetAction action: this.callback)
				action.execute();
		}
	}
}
