/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

public class ASPetUtils 
{
	/**
	 * Sort a map by value in ascending order
	 * @param map
	 * @return
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortValueAscending(Map<K, V> map)
	{
		List<Entry<K, V>> list = new LinkedList<>(map.entrySet());

		Comparator<Entry<K, V>> comparator = new Comparator<Entry<K, V>>()
		{
			@Override
			public int compare(Entry<K, V> one, Entry<K, V> two )
			{
				return ( one.getValue() ).compareTo( two.getValue() );
			}
		};

		Collections.sort(list, comparator);

		// Copy the new one to list
		 Map<K, V> result = new LinkedHashMap<>();
	    for (Map.Entry<K, V> entry : list)
	    {
	        result.put( entry.getKey(), entry.getValue() );
	    }
	    return result;
	}
	
	public static YamlConfiguration serializeStand(ArmorStand stand)
	{
		YamlConfiguration config = new YamlConfiguration();
		
		config.set("world", stand.getLocation().getWorld().getName());
		config.set("x", stand.getLocation().getX());
		config.set("y", stand.getLocation().getY());
		config.set("z", stand.getLocation().getZ());
		config.set("yaw", stand.getLocation().getYaw());
		config.set("pitch", stand.getLocation().getPitch());
		
		config.set("helmet", stand.getHelmet());
		config.set("chestplate", stand.getChestplate());
		config.set("leggings", stand.getLeggings());
		config.set("boots", stand.getBoots());
		config.set("righthand", stand.getItemInHand());
		// TODO: 1.9 left hand
		if (ASPetPlugin.useDualWield)
			config.set("lefthand", stand.getEquipment().getItemInOffHand());
		
		config.set("visible", stand.isVisible());
		config.set("arms", stand.hasArms());
		config.set("baseplate", stand.hasBasePlate());
		config.set("small", stand.isSmall());
		
		return config;
	}
	
	public static ArmorStand deserializeStand(ConfigurationSection config)
	{
		World world = Bukkit.getServer().getWorld(config.getString("world"));
		if (world == null)
			return null;
		
		Location loc = new Location(
				world,
				config.getDouble("x"),
				config.getDouble("y"),
				config.getDouble("z"),
				Float.parseFloat(config.getString("yaw")),
				Float.parseFloat(config.getString("pitch"))
				);
		
		ArmorStand stand = (ArmorStand)world.spawnEntity(loc, EntityType.ARMOR_STAND);
		stand.setHelmet(config.getItemStack("helmet"));
		stand.setChestplate(config.getItemStack("chestplate"));
		stand.setLeggings(config.getItemStack("leggings"));
		stand.setBoots(config.getItemStack("boots"));
		stand.setItemInHand(config.getItemStack("righthand"));
		if (ASPetPlugin.useDualWield)
			stand.getEquipment().setItemInOffHand(config.getItemStack("lefthand"));
		
		stand.setGravity(false);
		stand.setVisible(config.getBoolean("visible"));
		stand.setArms(config.getBoolean("arms"));
		stand.setBasePlate(config.getBoolean("baseplate"));
		stand.setSmall(config.getBoolean("small"));
		
		return stand;
	}
	
	public static ArmorStand respawnStand(ArmorStand original)
	{
		ArmorStand stand = (ArmorStand)original.getWorld().spawnEntity(original.getLocation(), EntityType.ARMOR_STAND);
		
		// Items
		stand.setHelmet(original.getHelmet());
		stand.setChestplate(original.getChestplate());
		stand.setLeggings(original.getLeggings());
		stand.setBoots(original.getBoots());
		stand.setItemInHand(original.getItemInHand());
		if (ASPetPlugin.useDualWield)
			stand.getEquipment().setItemInOffHand(original.getEquipment().getItemInOffHand());
		
		// Properties
		stand.setGravity(false);
		stand.setVisible(original.isVisible());
		stand.setArms(original.hasArms());
		stand.setBasePlate(original.hasBasePlate());
		stand.setSmall(original.isSmall());
		
		// Positioning
		stand.setLeftArmPose(original.getLeftArmPose());
		stand.setRightArmPose(original.getRightArmPose());
		stand.setLeftLegPose(original.getLeftLegPose());
		stand.setRightLegPose(original.getRightLegPose());
		stand.setHeadPose(original.getHeadPose());
		stand.setBodyPose(original.getBodyPose());
		
		original.remove();
		
		if (Settings.DEBUG)
		{
			ASPetPlugin.getInstance().getLogger().info("respawning armor stand at "
					+ stand.getLocation().getBlockX() + " "
					+ stand.getLocation().getBlockY() + " "
					+ stand.getLocation().getBlockZ());
		}
		
		return stand;
	}
}
