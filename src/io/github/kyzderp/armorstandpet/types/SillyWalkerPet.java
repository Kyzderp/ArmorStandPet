/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.types;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.DoneDitchingAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.ai.algorithms.PathFinder;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public class SillyWalkerPet extends Pet
{

	////////////////////////////// CONSTRUCTORS ///////////////////////////

	public SillyWalkerPet(ASPetPlugin plugin, String owner, ArmorStand stand)
	{
		super(plugin, owner, stand);

		this.type = PetType.SILLYWALKER;

		this.isMobile = true;
	}

	////////////////////////////// ACTIONS /////////////////////////////////

	public void ditch()
	{
		ASPetAction bye = new SayAction(this, 
				new DoneDitchingAction(this, new NotBusyAction(this, null)),
				"&eMaybe I need to practice my silly walks more.");

		this.isBusy = true;
		bye.execute();
	}

	/////////////////////////////// VERBAL /////////////////////////////////

	public String getInitialMessage()
	{
		return "&aHi, {owner}!";
	}

	/////////////////////////////// MOVEMENT ////////////////////////////////

	public void takeStep()
	{
		Location result = this.stand.getLocation().clone();
		Vector standDirection = result.getDirection();

		Vector walk = standDirection.normalize().multiply(this.speed);
		result.add(walk);

		// If it's inside a block, this is bad. Teleport up.
		if (!PathFinder.blockIsWalkable(result.getBlock()))
			result.setY(result.getBlockY() + 1);
		else // Teleport down to stay on the ground
			result.setY(result.getBlockY());

		this.stand.teleport(result);
	}

	///////////////////////////////////// ANIMATIONS //////////////////////////////

	/**
	 * Do the walking animation
	 */
	public void animateWalk()
	{
		this.walkStage++;
		if (this.walkStage > 8)
			this.walkStage = 1;

		switch(this.walkStage)
		{
		case 1: this.walk1(); break;
		case 2: this.walk2(); break;
		case 3: this.walk3(); break;
		case 4: this.walk4(); break;
		case 5: this.walk5(); break;
		case 6: this.walk6(); break;
		case 7: this.walk7(); break;
		case 8: this.walk8(); break;
		}

	}

	private void walk1()
	{
		this.stand.setLeftArmPose(new EulerAngle(0, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0, 0, -0.5));
		this.stand.setRightLegPose(new EulerAngle(0, 0, 0));
		this.stand.setBodyPose(new EulerAngle(0, 0, 0));
	}

	private void walk2()
	{
		this.stand.setLeftArmPose(new EulerAngle(0.2, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(-0.2, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(-0.8, 0, -0.4));
		this.stand.setRightLegPose(new EulerAngle(0.2, 0, 0));
	}

	private void walk3()
	{
		this.stand.setLeftArmPose(new EulerAngle(0.4, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(-0.4, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(-1.5, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(0.4, 0, 0));
	}

	private void walk4()
	{
		this.stand.setLeftArmPose(new EulerAngle(0.2, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(-0.2, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(-0.8, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(0.2, 0, 0.4));
	}

	private void walk5()
	{
		this.stand.setLeftArmPose(new EulerAngle(0, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(0, 0, 0.5));
		this.stand.setBodyPose(new EulerAngle(0, 0, 0));
	}

	private void walk6()
	{
		this.stand.setLeftArmPose(new EulerAngle(-0.2, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0.2, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0.2, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(-0.8, 0, 0.4));
	}

	private void walk7()
	{
		this.stand.setLeftArmPose(new EulerAngle(-0.4, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0.4, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0.4, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(-1.5, 0, 0));
	}

	private void walk8()
	{
		this.stand.setLeftArmPose(new EulerAngle(-0.2, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0.2, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0.2, 0, -0.4));
		this.stand.setRightLegPose(new EulerAngle(-0.8, 0, 0));
	}

	////////////////////////////////////// UTILITY ////////////////////////////////
}
