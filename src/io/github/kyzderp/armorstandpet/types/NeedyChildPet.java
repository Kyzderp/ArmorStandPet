/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.types;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.DoneDitchingAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.actions.SitAction;
import io.github.kyzderp.armorstandpet.ai.algorithms.PathFinder;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public class NeedyChildPet extends Pet
{

	////////////////////////////// CONSTRUCTORS ///////////////////////////

	public NeedyChildPet(ASPetPlugin plugin, String owner, ArmorStand stand)
	{
		super(plugin, owner, stand);

		this.type = PetType.NEEDYCHILD;

		if (this.stand != null && Settings.child_forceSmall)
		{
			this.stand.setSmall(true);
			this.plugin.getLogger().info("Pet automatically set to small size.");
		}

		this.isMobile = true;
	}

	////////////////////////////// ACTIONS /////////////////////////////////

	public void ditch()
	{
		ASPetAction afterMoreCrying = new DoneDitchingAction(this, new NotBusyAction(this, null));
		ASPetAction afterSit = new SayAction(this, afterMoreCrying, "&cWaaaaah!");
		ASPetAction afterCrying = new SitAction(this, afterSit);
		ASPetAction cry = new SayAction(this, afterCrying, "&cWhat?! You're leaving me?!");

		this.isBusy = true;
		cry.execute();
	}

	/////////////////////////////// VERBAL /////////////////////////////////

	public String getInitialMessage()
	{
		return "&aHi, {owner}!";
	}

	/////////////////////////////// MOVEMENT ////////////////////////////////

	public void takeStep()
	{
		Location result = this.stand.getLocation().clone();
		Vector standDirection = result.getDirection();

		Vector walk = standDirection.normalize().multiply(this.speed);
		result.add(walk);

		// If it's inside a block, this is bad. Teleport up.
		if (!PathFinder.blockIsWalkable(result.getBlock()))
			result.setY(result.getBlockY() + 1);
		else // Teleport down to stay on the ground
			result.setY(result.getBlockY());

		this.stand.teleport(result);
	}

	///////////////////////////////////// ANIMATIONS //////////////////////////////

	/**
	 * Do the walking animation
	 */
	public void animateWalk()
	{
		this.walkStage++;
		if (this.walkStage > 8)
			this.walkStage = 1;

		switch(this.walkStage)
		{
		case 1: this.walkFlat(); break;
		case 2: this.walkSmallLeftForward(); break;
		case 3: this.walkBigLeftForward(); break;
		case 4: this.walkSmallLeftForward(); break;
		case 5: this.walkFlat(); break;
		case 6: this.walkSmallRightForward(); break;
		case 7: this.walkBigRightForward(); break;
		case 8: this.walkSmallRightForward(); break;
		}

	}



	private void walkSmallLeftForward()
	{
		this.stand.setLeftArmPose(new EulerAngle(0.2, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(-0.2, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(-0.2, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(0.2, 0, 0));
	}

	private void walkBigLeftForward()
	{
		this.stand.setLeftArmPose(new EulerAngle(0.4, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(-0.4, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(-0.4, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(0.4, 0, 0));
	}

	private void walkSmallRightForward()
	{
		this.stand.setLeftArmPose(new EulerAngle(-0.2, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0.2, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0.2, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(-0.2, 0, 0));
	}

	private void walkBigRightForward()
	{
		this.stand.setLeftArmPose(new EulerAngle(-0.4, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0.4, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0.4, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(-0.4, 0, 0));
	}

	////////////////////////////////////SERIALIZE ///////////////////////////////////
}
