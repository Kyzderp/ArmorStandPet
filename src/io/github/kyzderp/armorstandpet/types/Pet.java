/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.types;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.ASPetUtils;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.ai.algorithms.PathFinder;
import io.github.kyzderp.armorstandpet.storage.PetSettings;
import io.github.kyzderp.armorstandpet.struct.OwnerToPet;
import io.github.kyzderp.armorstandpet.struct.StandToOwner;
import io.github.kyzderp.armorstandpet.tasks.RevertNameTask;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public abstract class Pet
{
	// Core fields
	protected ASPetPlugin plugin;
	protected String owner;
	protected ArmorStand stand;
	public PetType type;

	// Is the pet in the middle of doing something?
	public boolean isBusy;
	// Is the pet busy sitting?
	public boolean isSitting;

	// Can pet fly?
	public boolean isFlying;

	// Can this pet walk?
	public boolean isMobile;

	// Pet attributes
	protected double speed;
	protected String name; 

	// Which stage of the walking animation is this armorstand in?
	protected int walkStage;

	// Possible sayings
	public List<String> announces = Settings.defaultAnnounces;
	public List<String> insults = Settings.defaultInsults;


	////////////////////////////// CONSTRUCTORS ///////////////////////////

	public Pet(ASPetPlugin plugin, String owner, ArmorStand stand)
	{
		this.plugin = plugin;
		this.owner = owner;
		this.stand = stand;
		this.isBusy = false;
		this.isSitting = false;
		this.isMobile = false;
		this.speed = 0.3;

		this.name = "";

		if (this.stand != null)
		{
			this.stand.setGravity(false);
			if (Settings.forceArms)
				this.stand.setArms(true);
			if (Settings.forceNoPlate)
				this.stand.setBasePlate(false);
		}
	}

	// TODO: bother the player, run in circles, bored
	// TODO: pets interact with each other, if it's bored, dance?
	// TODO: throw snowballs?
	// TODO: sing when bored
	// TODO: punch

	// TODO: config option whether to allow edits or not --> maybe be able to change skull?

	// TODO: needs original position <-- no idea what i was thinking about when i wrote that
	// TODO: when player clicks on doorman?

	// TODO: be able to tell it to go to x y z?

	// TODO: have multiple pets

	// TODO: silly walker choose different type of walk every time

	////////////////////////////// ACTIONS /////////////////////////////////

	public abstract void ditch();

	/////////////////////////////// VERBAL /////////////////////////////////

	public void say(String message)
	{
		this.say(message, null);
	}

	public void sayTarget(String message, String target)
	{
		this.say(message.replaceAll("\\{target\\}", target));
	}

	public void say(String message, ASPetAction callback)
	{
		(new SayAction(this, callback, message)).execute();
	}

	public abstract String getInitialMessage();

	/////////////////////////////// MOVEMENT ////////////////////////////////

	public void faceOwner()
	{
		this.facePlayer(Bukkit.getPlayer(this.owner));
	}

	public void facePlayer(Player player)
	{
		if (player != null && player.isOnline())
		{
			Location result = this.stand.getLocation().clone();
			Vector standLocVector = result.toVector();

			Vector difference = player.getLocation().toVector().subtract(standLocVector);
			result.setDirection(difference);

			this.stand.teleport(result);
			this.lookAtPlayer(player);
		}
	}

	public void faceLoc(Location loc)
	{
		if (loc != null)
		{
			Location result = this.stand.getLocation().clone();
			Vector standLocVector = result.toVector();

			Vector difference = loc.toVector().subtract(standLocVector);
			result.setDirection(difference);

			this.stand.teleport(result);
		}
	}

	/**
	 * Make the armor stand's head look up or down to look at player's eye height
	 * @param player
	 */
	public void lookAtPlayer(Player player)
	{
		Vector playerEye = player.getEyeLocation().clone().toVector();
		Vector standEye = this.stand.getEyeLocation().clone().toVector();

		Vector difference = playerEye.subtract(standEye);

		// this should give same-height vector
		Vector flat = difference.clone().setY(0);

		// Now need angle between them
		float angle = flat.angle(difference);
		if (difference.getY() > 0)
			angle = -angle;
		this.stand.setHeadPose(new EulerAngle(angle, 0, 0));
	}

	public abstract void takeStep();

	public void walkFlat()
	{
		this.stand.setLeftArmPose(new EulerAngle(0, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(0, 0, 0));
		this.stand.setBodyPose(new EulerAngle(0, 0, 0));
		//		this.stand.setHeadPose(new EulerAngle(0, 0, 0));
	}

	///////////////////////////////////// ANIMATIONS //////////////////////////////

	/**
	 * Sit and teleport down
	 */
	public void sit()
	{
		this.stand.setLeftLegPose(new EulerAngle(1.5 * Math.PI, -0.15, 0));
		this.stand.setRightLegPose(new EulerAngle(1.5 * Math.PI, 0.1, 0));
		this.stand.setLeftArmPose(new EulerAngle(0, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0, 0, 0.1));

		Location loc = this.stand.getLocation().clone();
		// If it's inside a block, this is bad. Teleport up.
		if (!PathFinder.blockIsWalkable(loc.getBlock()))
			loc.setY(loc.getBlockY() + 1);
		else // Teleport down to stay on the ground
			loc.setY(loc.getBlockY());

		if (this.stand.isSmall())
			this.stand.teleport(new Location(loc.getWorld(), 
					loc.getX(), loc.getBlockY() - 0.3, loc.getZ(), loc.getYaw(), loc.getPitch()));
		else
			this.stand.teleport(new Location(loc.getWorld(), 
					loc.getX(), loc.getBlockY() - 0.6, loc.getZ(), loc.getYaw(), loc.getPitch()));
		this.isSitting = true;
	}

	/**
	 * Stand upright and teleport to a suitable "ground" block
	 */
	public void stand()
	{
		if (this.isSitting)
			this.nearestBlock();

		this.stand.setLeftArmPose(new EulerAngle(0, 0, -0.1));
		this.stand.setRightArmPose(new EulerAngle(0, 0, 0.1));
		this.stand.setLeftLegPose(new EulerAngle(0, 0, 0));
		this.stand.setRightLegPose(new EulerAngle(0, 0, 0));
		this.stand.setBodyPose(new EulerAngle(0, 0, 0));
		this.stand.setHeadPose(new EulerAngle(0, 0, 0));
		this.isSitting = false;
	}

	/** 
	 * Teleport up/down to nearest block
	 */
	public void nearestBlock()
	{
		Location loc = this.stand.getLocation();
		if (loc.getBlock().isEmpty())
			this.stand.teleport(new Location(loc.getWorld(), 
					loc.getX(), loc.getBlockY(), loc.getZ(), loc.getYaw(), loc.getPitch()));
		else
			this.stand.teleport(new Location(loc.getWorld(), 
					loc.getX(), loc.getBlockY() + 1, loc.getZ(), loc.getYaw(), loc.getPitch()));
	}

	/**
	 * Raise arms to hug
	 */
	@SuppressWarnings("deprecation")
	public void hug()
	{
		this.stand.setLeftArmPose(new EulerAngle(1.7 * Math.PI, -Math.PI / 6, 0));
		this.stand.setRightArmPose(new EulerAngle(1.7 * Math.PI, Math.PI / 6, 0));
		this.stand.getLocation().getWorld().playEffect(this.stand.getLocation().add(0, 1, 0),
				Effect.HEART, 0);
	}

	/**
	 * Do the walking animation
	 */
	public abstract void animateWalk();

	///////////////////////////// UTILITY METHODS ////////////////////////////

	public double distanceSq(Player player)
	{
		return this.stand.getLocation().distanceSquared(player.getLocation());
	}

	public double distanceSq(Location loc)
	{
		return this.stand.getLocation().distanceSquared(loc);
	}

	protected String getRandom(List<String> possibles)
	{
		if (possibles.size() == 0)
			return "";
		Random rand = new Random();

		return possibles.get(rand.nextInt(possibles.size()));
	}

	/**
	 * Get a random announcement
	 * @param target
	 * @return
	 */
	public String getAnnounce(String target)
	{
		return this.getRandom(this.announces).replaceAll("\\{target\\}", target);
	}

	/**
	 * Get a random insult
	 * @param target
	 * @return
	 */
	public String getInsult(String target)
	{
		return this.getRandom(this.insults).replaceAll("\\{target\\}", target);
	}

	/**
	 * Displays the message
	 * @param message
	 * @param callback
	 */
	public void displayMessage(String message, List<ASPetAction> callback)
	{
		if (message.equals(""))
		{
			for (ASPetAction action: callback)
				action.execute();
			return;
		}

		message = message.replaceAll("&", "\u00A7");
		message = message.replaceAll("\\{owner\\}", this.owner);

		this.stand.setCustomName(message);
		stand.setCustomNameVisible(true);
		(new RevertNameTask(this, callback)).runTaskLater(this.plugin, 60);
	}

	public String getLocationString()
	{
		return this.stand.getLocation().getBlockX() + " " + this.stand.getLocation().getBlockY()
				+ " " + this.stand.getLocation().getBlockZ();
	}

	public void teleportTo(Player player)
	{
		this.getStand().teleport(player.getLocation());
	}

	public void teleportTo(Location loc)
	{
		this.getStand().getWorld().playEffect(this.getStand().getLocation().add(0, 1, 0), 
				Effect.ENDER_SIGNAL, 0);
		this.getStand().teleport(loc);
		loc.getWorld().playEffect(loc.add(0, 1, 0), 
				Effect.ENDER_SIGNAL, 0);
	}

	public void teleportToFrontOf(Location loc, double distance)
	{
		Location l = loc.clone();
		l.setPitch(0);

		Vector vec = l.getDirection();
		vec.setY(0);
		vec.multiply(distance);

		l.add(vec);
		this.getStand().teleport(l);
	}

	public void delete(boolean save, boolean delete)
	{
		String worldname = this.getStand().getWorld().getName();
		// Remove from the maps
		StandToOwner.remove(worldname, this.getStand());
		OwnerToPet.remove(worldname, this.owner);

		if (delete)
			this.getStand().remove();

		if (save)
			this.plugin.savePets(new PetSettings(this.owner, this, worldname));
	}

	///////////////////////////////// GETTERS /////////////////////////////////
	public String getOwner()
	{
		return this.owner;
	}

	public ArmorStand getStand()
	{
		return this.stand;
	}

	public String getName()
	{
		return this.name;
	}

	///////////////////////////////// SETTERS /////////////////////////////////

	public void setSpeed(double speed)
	{
		this.speed = speed;
	}

	public void setName(String name)
	{
		this.name = name.replaceAll("&", "\u00A7");
	}

	public void setStand(ArmorStand stand)
	{
		this.stand = stand;
	}

	public void setOwner(String owner)
	{
		this.owner = owner;
	}

	///////////////////////////////// MAKE NEW ////////////////////////////////
	public static Pet createPet(PetType type, String owner, ArmorStand stand)
	{
		if (type == PetType.NEEDYCHILD)
			return new NeedyChildPet(ASPetPlugin.getInstance(), owner, stand);
		if (type == PetType.DOORMAN)
			return new DoormanPet(ASPetPlugin.getInstance(), owner, stand);
		if (type == PetType.SILLYWALKER)
			return new SillyWalkerPet(ASPetPlugin.getInstance(), owner, stand);
		if (type == PetType.DEMON)
			return new DemonPet(ASPetPlugin.getInstance(), owner, stand);
		return null;
	}

	////////////////////////////////////SERIALIZE ///////////////////////////////////

	public ConfigurationSection serialize()
	{
		ConfigurationSection config = new YamlConfiguration();

		config.set("type", this.type.name);
		config.set("owner", this.owner);
		config.set("name", this.name);
		config.set("speed", this.speed);
		config.set("announces", this.announces);
		config.set("insults", this.insults);

		config.set("stand", ASPetUtils.serializeStand(this.stand));

		return config;
	}

	public void deserialize(ConfigurationSection config)
	{
		this.owner = config.getString("owner");
		this.name = config.getString("name");
		this.speed = config.getDouble("speed");
		this.announces = config.getStringList("announces");
		this.insults = config.getStringList("insults");

		this.stand = ASPetUtils.deserializeStand(config.getConfigurationSection("stand"));
		if (this.stand == null)
			return;
		if (this.isSitting)
			this.sit();
		(new RevertNameTask(this, (ASPetAction)null)).run();
	}

	public void deserializeSettings(ConfigurationSection config)
	{
		this.owner = config.getString("owner");
		this.name = config.getString("name");
		this.speed = config.getDouble("speed");
		this.announces = config.getStringList("announces");
		this.insults = config.getStringList("insults");
		
		this.stand.setGravity(false);
		this.stand.setVisible(config.getBoolean("stand.visible"));
		this.stand.setArms(config.getBoolean("stand.arms"));
		this.stand.setBasePlate(config.getBoolean("stand.baseplate"));
		this.stand.setSmall(config.getBoolean("stand.small"));
		
		if (this.isSitting)
			this.sit();
		(new RevertNameTask(this, (ASPetAction)null)).run();
	}
}
