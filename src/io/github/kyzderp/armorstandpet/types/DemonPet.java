/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.types;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.actions.ASPetAction;
import io.github.kyzderp.armorstandpet.actions.DoneDitchingAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.SayAction;
import io.github.kyzderp.armorstandpet.ai.algorithms.PathFinder;
import io.github.kyzderp.armorstandpet.tasks.RevertNameTask;

import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public class DemonPet extends Pet
{
	////////////////////////////// CONSTRUCTORS ///////////////////////////

	public DemonPet(ASPetPlugin plugin, String owner, ArmorStand stand)
	{
		super(plugin, owner, stand);

		this.isMobile = true;
		this.type = PetType.DEMON;
	}

	////////////////////////////// ACTIONS /////////////////////////////////

	public void ditch()
	{
		ASPetAction bye = new SayAction(this, 
				new DoneDitchingAction(this, new NotBusyAction(this, null)), 
				"&eOkay, bye.");

		this.isBusy = true;
		bye.execute();
	}

	/////////////////////////////// VERBAL /////////////////////////////////

	public String getInitialMessage()
	{
		return "&aHi, {owner}!";
	}

	/////////////////////////////// MOVEMENT ////////////////////////////////

	public void takeStep()
	{
		Location result = this.stand.getLocation().clone();
		Vector standDirection = result.getDirection();

		Vector walk = standDirection.normalize().multiply(this.speed);
		result.add(walk);

		// If it's inside a block, this is bad. Teleport up.
		if (!PathFinder.blockIsWalkable(result.getBlock()))
			result.setY(result.getBlockY() + 1);
		else // Teleport down to stay on the ground
			result.setY(result.getBlockY());

		this.stand.teleport(result);
	}

	///////////////////////////////////// ANIMATIONS //////////////////////////////

	/**
	 * Do the walking animation
	 */
	@SuppressWarnings("deprecation")
	public void animateWalk()
	{
		this.stand.setHeadPose(this.randomAngle());
		this.stand.setBodyPose(this.randomAngle());
		this.stand.setLeftArmPose(this.randomAngle());
		this.stand.setRightArmPose(this.randomAngle());
		this.stand.setLeftLegPose(this.randomAngle());
		this.stand.setRightLegPose(this.randomAngle());

		this.stand.getLocation().getWorld().playEffect(this.stand.getLocation(),
				Effect.FLYING_GLYPH, 0);
		this.stand.getLocation().getWorld().playEffect(this.stand.getLocation(),
				Effect.FLAME, 0);
	}

	////////////////////////////////////// UTILITY ////////////////////////////////
	private EulerAngle randomAngle()
	{
		return new EulerAngle(Math.random() * Math.PI * 2,
				Math.random() * Math.PI * 2, 
				Math.random() * Math.PI * 2);
	}

	@Override
	public void displayMessage(String message, List<ASPetAction> callback)
	{
		message = message.replaceAll("&", "\u00A7");
		message = message.replaceAll("\\{owner\\}", this.owner);

		if (message.contains("\u00A7"))
		{
			char firstColor = message.charAt(message.indexOf("\u00A7") + 1);
			message = message.replaceFirst("\u00A7.", "\u00A7" + firstColor + "\u00A7k");
		}
		else
		{
			message = "\u00A7k" + message;
		}

		this.stand.setCustomName(message);
		stand.setCustomNameVisible(true);
		(new RevertNameTask(this, callback)).runTaskLater(this.plugin, 60);
	}
}
