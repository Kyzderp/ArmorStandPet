/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.types;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.actions.DoneDitchingAction;
import io.github.kyzderp.armorstandpet.actions.NotBusyAction;
import io.github.kyzderp.armorstandpet.actions.StandAction;
import io.github.kyzderp.armorstandpet.hooks.HookManager;
import io.github.kyzderp.armorstandpet.tasks.RemoveCooldownTask;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.util.EulerAngle;

public class DoormanPet extends Pet
{
	public List<String> greetings;
	private double greetRange;
	private HashSet<String> greetCooldown;

	////////////////////////////// CONSTRUCTORS ///////////////////////////

	public DoormanPet(ASPetPlugin plugin, String owner, ArmorStand stand)
	{
		super(plugin, owner, stand);

		this.type = PetType.DOORMAN;

		this.isMobile = false;
		this.greetRange = 5.0;

		this.greetings = new ArrayList<String>();
		this.greetings.add("&aWelcome, {target}!");

		this.greetCooldown = new HashSet<String>();
	}

	////////////////////////////// ACTIONS /////////////////////////////////

	public void greet(Player player)
	{
		// Don't greet players that have already been greeted
		if (this.greetCooldown.contains(player.getName()))
			return;

		// Do not greet vanished players, but still add cooldown
		if (!HookManager.vanishCheck.isVanished(player))
		{
			this.isBusy = true;
			this.facePlayer(player);
			this.stand.setRightArmPose(new EulerAngle(4, 0, -1));

			// After greeting, doorman should go back to standing
			StandAction standStraight = new StandAction(this, new NotBusyAction(this, null));
			this.say(this.getGreeting(player.getName()), standStraight);
		}

		this.greetCooldown.add(player.getName());
		(new RemoveCooldownTask(this, player.getName())).runTaskLater(this.plugin, Settings.doorman_cooldown);
	}

	public void ditch()
	{
		this.isBusy = true;
		this.stand();
		this.say("&aGlad to be of service.", 
				new DoneDitchingAction(this, new NotBusyAction(this, null)));
	}

	/////////////////////////////// VERBAL /////////////////////////////////

	public String getInitialMessage()
	{
		return "&aAt your service, {owner}.";
	}

	public String getGreeting(String target)
	{
		return this.getRandom(this.greetings).replaceAll("\\{target\\}", target);
	}

	/////////////////////////////// MOVEMENT ////////////////////////////////

	public void takeStep()
	{
		// Do nothing
	}

	///////////////////////////////////// ANIMATIONS //////////////////////////////

	/**
	 * Puts arms straight down instead of slightly outwards, more "professional"
	 */
	public void stand()
	{
		super.stand();
		this.stand.setLeftArmPose(new EulerAngle(0, 0, 0));
		this.stand.setRightArmPose(new EulerAngle(0, 0, 0));
	}

	/**
	 * Do the walking animation
	 */
	public void animateWalk()
	{
		// Do nothing
	}

	////////////////////////////////////// GETTERS ETC /////////////////////////////////

	public double getRange()
	{
		return this.greetRange;
	}

	public void setRange(double range)
	{
		this.greetRange = range;
	}

	public void removeCooldown(String name)
	{
		this.greetCooldown.remove(name);
	}

	//////////////////////////////////// SERIALIZE ///////////////////////////////////

	@Override
	public ConfigurationSection serialize()
	{
		ConfigurationSection config = super.serialize();

		config.set("greetings", this.greetings);
		config.set("greetrange", this.greetRange);

		return config;
	}

	@Override
	public void deserialize(ConfigurationSection config)
	{
		super.deserialize(config);

		this.greetings = config.getStringList("greetings");
		this.greetRange = config.getDouble("greetrange");
	}

	@Override
	public void deserializeSettings(ConfigurationSection config)
	{
		super.deserializeSettings(config);

		this.greetings = config.getStringList("greetings");
		this.greetRange = config.getDouble("greetrange");
	}
}
