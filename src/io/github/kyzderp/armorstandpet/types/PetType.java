/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.types;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

public enum PetType 
{
	NEEDYCHILD("Needy Child", Material.APPLE, new String[] {"This needy child", "follows you around."}),
	DOORMAN("Doorman", Material.WOOD_DOOR, new String[] {"The doorman greets", "people for you."}),
	SILLYWALKER("Silly Walker", Material.STICK, new String[] {"Fresh from the", "Ministry of Silly Walks."}),
	DEMON("Demon", Material.EYE_OF_ENDER, new String[] {"The devil seems to have", "possessed your pet."});
	
	public String name;
	public Material material;
	public List<String> lore;
	public boolean enabled;

	private PetType(String name, Material material, String lore)
	{
		this.name = name;
		this.material = material;
		this.lore = new ArrayList<String>();
		this.lore.add("\u00A7r\u00A7b" + lore);
	}
	
	private PetType(String name, Material material, String[] lore)
	{
		this.name = name;
		this.material = material;
		this.lore = new ArrayList<String>();
		for (String line: lore)
			this.lore.add("\u00A7r\u00A7b" + line);
	}
	
	public static PetType getTypeByName(String name)
	{
		for (PetType type: PetType.values())
		{
			if (type.name.equals(name))
				return type;
		}
		return null;
	}
}
