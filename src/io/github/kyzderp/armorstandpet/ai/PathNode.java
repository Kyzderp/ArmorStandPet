/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.ai;

import io.github.kyzderp.armorstandpet.ai.algorithms.PathFinder;

import org.bukkit.Location;
import org.bukkit.World;


public class PathNode 
{
	public int x;
	public int y;
	public int z;

	public int gScore;
	public int hScore;

	public PathNode parent;

	public PathNode(int x, int y, int z, PathNode parent, PathNode target)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.parent = parent;

		if (parent != null)
			this.gScore = parent.gScore + PathFinder.horizontalCost;

		// Manhattan method
		if (target != null)
			this.hScore = Math.abs(target.x - this.x) 
			+ Math.abs(target.y - this.y)
			+ Math.abs(target.z - this.z);

//		if (Settings.DEBUG)
//			ASPetPlugin.getInstance().getLogger().info("Created new PathNode " + x + " " + y + " " + z);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof PathNode))
			return false;
		PathNode other = (PathNode) obj;
		if (this.x == other.x && this.y == other.y && this.z == other.z)
			return true;
		return false;
	}

	public double distSq(PathNode other)
	{
		return Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2)
				+ Math.pow(this.z - other.z, 2);
	}

	public String getString()
	{
		return this.x + " " + this.y + " " + this.z;
	}

	public Location getLocation(World world)
	{
		return new Location(world, this.x + 0.5, this.y, this.z + 0.5);
	}
}
