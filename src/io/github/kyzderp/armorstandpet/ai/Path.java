/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.ai;

import java.util.List;

import org.bukkit.Location;

public class Path 
{
	public List<PathNode> nodes;
	public Location start;
	public Location end;
	public boolean isSame = false;
	
	public Path(Location start, Location end, List<PathNode> nodes)
	{
		this.start = start;
		this.end = end;
		this.nodes = nodes;
	}
	
	public String dump()
	{
		String result = "Path: ";
		if (this.nodes == null)
			result = "NO PATH";
		else
			for (PathNode node: this.nodes)
			{
				result += " {" + node.getString() + "}";
			}
		return result;
	}
}
