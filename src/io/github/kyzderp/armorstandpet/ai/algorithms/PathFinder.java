/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.ai.algorithms;

import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.ai.Path;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public abstract class PathFinder 
{
	public static int horizontalCost = 10;
	public static int verticalCost = 5;

	/**
	 * Find and return a path
	 * @return a Path object representing the found path. If no path is found, the Path object's PathNode list will be null.
	 */
	public abstract Path findPath();


	public static PathFinder getPathfinder(Location source, Location target, String type)
	{
		if (type.equalsIgnoreCase("AStar"))
			return new AStar(source, target);
		// Default
		return new AStar(source, target);	
	}

	public static boolean blockIsWalkable(Block block)
	{
		Material type = block.getType();
		if (Settings.nonSolid.contains(type))
			return true;

		return !type.isSolid();
		//		return block.isEmpty();
	}
	
/*	public static boolean blockUnderIsSolid(Block block)
	{
		if (Settings.floorSolid.contains(type))
			return true;
		return false;
	}*/
}
