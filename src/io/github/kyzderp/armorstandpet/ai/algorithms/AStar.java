/*******************************************************************************
 * Copyright (c) 2016, 2017 Hannah Chu
 * All rights reserved. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Yi-Hsueh (Hannah) Chu <hannah_chu@yahoo.com> 2016-2017
 *******************************************************************************/
package io.github.kyzderp.armorstandpet.ai.algorithms;

import io.github.kyzderp.armorstandpet.ASPetPlugin;
import io.github.kyzderp.armorstandpet.Settings;
import io.github.kyzderp.armorstandpet.ai.Path;
import io.github.kyzderp.armorstandpet.ai.PathNode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;

public class AStar extends PathFinder 
{
	public class PathNodeComparator implements Comparator<PathNode>
	{
		@Override
		public int compare(PathNode o1, PathNode o2) 
		{
			return (o1.gScore + o1.hScore) - (o2.gScore + o2.hScore);
		}
	}

	private World world;
	private PathNode start;
	private PathNode end;

	private PriorityQueue<PathNode> openQueue;
	private Map<String, PathNode> openSet;
	private Set<String> closedSet;

	public AStar(Location source, Location target)
	{
		this.world = source.getWorld();
		if (!this.world.equals(target.getWorld()))
			this.world = null;

		this.end = this.findBottom(target.getBlockX(),
				target.getBlockY(),
				target.getBlockZ(),
				this.world, null);
		
		// In case under the world
		if (this.end == null)
			this.end = this.findTop(target.getBlockX(),
					target.getBlockY(),
					target.getBlockZ(),
					this.world, null);
		
		// In case completely void
		if (this.end == null)
			this.end = new PathNode(target.getBlockX(), 
					target.getBlockY(), 
					target.getBlockZ(), 
					null, null);
		
		
		this.start = this.findTop(source.getBlockX(), 
				source.getBlockY(), 
				source.getBlockZ(),
				this.world, null);
		
		// In case above the world
		if (this.start == null)
			this.start = this.findBottom(source.getBlockX(), 
					source.getBlockY(), 
					source.getBlockZ(),
					this.world, null);
		
		// In case completely void
		if (this.start == null)
			this.start = new PathNode(source.getBlockX(), 
					source.getBlockY(), 
					source.getBlockZ(), 
					null, null);

		// The open list of nodes to check
		this.openQueue = new PriorityQueue<PathNode>(100, new PathNodeComparator());
		this.openQueue.add(this.start);

		this.openSet = new HashMap<String, PathNode>();
		this.openSet.put(this.start.getString(), this.start);

		// Not needed to check again
		this.closedSet = new HashSet<String>();
	}


	@Override
	public Path findPath() 
	{
		long time = System.currentTimeMillis();
		
		if (Settings.DEBUG)
		{
			ASPetPlugin.getInstance().getLogger().info("Start is " + this.start.getString());
			ASPetPlugin.getInstance().getLogger().info("End is " + this.end.getString());
		}

		// Different world
		if (this.world == null)
			return new Path(this.start.getLocation(this.world), 
					this.end.getLocation(this.world), null);

		// I'm already there~
		if (this.start.equals(this.end))
		{
			Path path = new Path(this.start.getLocation(this.world), 
					this.end.getLocation(this.world), null);
			path.isSame = true;
			return path;
		}
		
		// Too far, just say no path
		if (this.start.getLocation(this.world).distanceSquared(
				this.end.getLocation(this.world)) > Settings.teleportRange)
			return new Path(this.start.getLocation(this.world), 
					this.end.getLocation(this.world), null);

		// Main part
		while (!this.openQueue.isEmpty())
		{
			PathNode currNode = this.openQueue.poll();
			String currNodeString = currNode.getString();
			this.openSet.remove(currNodeString);

			this.closedSet.add(currNode.getString());
			if (currNode.equals(this.end))
			{
				this.end = currNode;
				return this.constructPath();
			}

			int[][] neighbors = new int[][] {
					new int[] {currNode.x+1, currNode.y, currNode.z},
					new int[] {currNode.x-1, currNode.y, currNode.z},
					new int[] {currNode.x, currNode.y, currNode.z+1},
					new int[] {currNode.x, currNode.y, currNode.z-1}
			};
			// Now go through all of the neighbors
			for (int[] coords: neighbors)
			{
				int x = coords[0];
				int y = coords[1];
				int z = coords[2];
				
				PathNode neighborNode;
				
				// First check if we're going too far
				if (this.outOfBounds(x, y, z))
				{
					this.closedSet.add(x + " " + y + " " + z);
					continue;
				}
				
				if (PathFinder.blockIsWalkable(this.world.getBlockAt(x, y, z)))
				{
					// The adjacent block is available, so find the lowest (drop down)
					neighborNode = this.findBelow(x, y, z, this.world, currNode);
				}
				else
				{
					// Have to go up
					neighborNode = this.findAbove(x, y, z, this.world, currNode);
				}

				// This one is already closed, skip
				if (neighborNode == null)
				{
					this.closedSet.add(x + " " + y + " " + z);
					continue;
				}
				else if (this.closedSet.contains(neighborNode.getString()))
				{
					continue;
				}

				int newGScore = currNode.gScore + PathFinder.horizontalCost
						+ Math.abs(y - neighborNode.y) * PathFinder.verticalCost;


				if (!this.openSet.containsKey(neighborNode.getString()))
				{
					this.openQueue.add(neighborNode);
					this.openSet.put(neighborNode.getString(), neighborNode);
					//					if (Settings.DEBUG)
					//						ASPetPlugin.getInstance().getLogger().info("Added " + neighborNode.getString());
				}
				else
				{
					// This is an old node
					PathNode oldNode = this.openSet.get(neighborNode.getString());

					// This path isn't better
					if (newGScore >= oldNode.gScore)
						continue;

					// This path is better
					oldNode.parent = currNode;
					oldNode.gScore = newGScore;
				}

			}
		}

		time = System.currentTimeMillis() - time;
		if (Settings.DEBUG)
			ASPetPlugin.getInstance().getLogger().info("No path found in " + time + " milliseconds.");
		return new Path(this.start.getLocation(this.world), 
				this.end.getLocation(this.world), null);
	}

	/**
	 * Construct the return path after having calculated the nodes 
	 * @return
	 */
	private Path constructPath()
	{
		if (Settings.DEBUG)
			ASPetPlugin.getInstance().getLogger().info("Inside construct path");
		PathNode curr = this.end;
		List<PathNode> toReturn = new ArrayList<PathNode>();
		while (!curr.equals(this.start))
		{
			toReturn.add(0, curr);
			curr = curr.parent;
			if (curr.parent == null)
				break;
		}
		toReturn.add(0, curr);

		return new Path(this.start.getLocation(this.world),
				this.end.getLocation(this.world),
				toReturn);
	}
	
	/**
	 * Check if the block is out of bounds for the pathfinder
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	private boolean outOfBounds(int x, int y, int z)
	{
		// Too far away
		if (Math.abs(x - this.start.x) > Settings.maxPathRadius
				|| Math.abs(z - this.start.z) > Settings.maxPathRadius)
			return true;
		return false;
	}
	
	/**
	 * Find the ground below the coordinates
	 * @param x
	 * @param y
	 * @param z
	 * @param world
	 * @param parent
	 * @return
	 */
	private PathNode findBottom(int x, int y, int z, World world, PathNode parent)
	{
		while (y >= 0)
		{
			if (PathFinder.blockIsWalkable(world.getBlockAt(x, y, z)))
				y--;
			else // Found a ground block
			{
				return new PathNode(x, y + 1, z, parent, this.end);
			}
		}
		
		return null;
	}
	
	/**
	 * See if the block is walkable, or go down 3 blocks
	 * @param x
	 * @param y
	 * @param z
	 * @param world
	 * @param parent
	 * @return
	 */
	private PathNode findBelow(int x, int y, int z, World world, PathNode parent)
	{
		int i = 0;
		while (i < 5) // Only willing to go down 3 blocks
		{
			if (PathFinder.blockIsWalkable(world.getBlockAt(x, y, z)))
				y--;
			else // Found a ground block
			{
				return new PathNode(x, y + 1, z, parent, this.end);
			}
			i++;
		}
		return null;
	}

	/**
	 * See if the block is walkable, or the block above it is
	 * @param x
	 * @param y
	 * @param z
	 * @param world
	 * @param parent
	 * @return
	 */
	private PathNode findAbove(int x, int y, int z, World world, PathNode parent)
	{
		// Check only the block and the block above
		if (PathFinder.blockIsWalkable(world.getBlockAt(x, y, z)))
			return new PathNode(x, y, z, parent, this.end);
		
		if (PathFinder.blockIsWalkable(world.getBlockAt(x, y + 1, z)))
			return new PathNode(x, y + 1, z, parent, this.end);
		
		return null;
	}
	
	/**
	 * Find the ground above the coordinates
	 * @param x
	 * @param y
	 * @param z
	 * @param world
	 * @param parent
	 * @return
	 */
	private PathNode findTop(int x, int y, int z, World world, PathNode parent)
	{
		while (y < 256)
		{
			if (PathFinder.blockIsWalkable(world.getBlockAt(x, y, z)))
				return new PathNode(x, y, z, parent, this.end);
			else
				y++;
		}
		return null;
	}
}
